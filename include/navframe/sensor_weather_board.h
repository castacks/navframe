/*
 * sensor_weather_board.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#include <navframe/sensor_generic.h>
#include <spk_weather_board/AirDensityHeight.h>

#ifndef SENSOR_WEATHER_BOARD_H_
#define SENSOR_WEATHER_BOARD_H_


namespace ca
{

class SensorWeatherBoard : public SensorGeneric
{

public:
  double baro_std;
  double baro_drift_std;
  boost::shared_ptr<diagnostic_updater::TopicDiagnostic> baroDiag;

  SensorWeatherBoard()
  {
    reset();
  };

  void reset(void)
  {
    baro_std = -1;
    baro_drift_std = -1;
  }


  bool storeSensorMsg(filtInst &ss, const spk_weather_board::AirDensityHeight::ConstPtr& msg, bool hasNewFrame)
  {
    //ROS_INFO("spk_weather_board::AirDensityHeight msg received");
    ss.heightMsg = msg;
    isAlive = true;
    if(baroDiag) baroDiag->tick(msg->header.stamp);
    return true;
  }


  bool processSensorMsg(const spk_weather_board::AirDensityHeight::ConstPtr& msg, ImuVo& filt)
  {
    bool success=false;
    if (!isInitialized || abs(filt.x(BARO_OFFSET)) < 1e-12)
    {
      filt.x(BARO_OFFSET) = msg->height_m + filt.x(POSITION+2);
      ROS_INFO_STREAM("Initializing barometer... ");
      ROS_INFO_STREAM("Baro: " << msg->height_m << " Z: " << filt.x(POSITION+2) << " Offset: " << filt.x(BARO_OFFSET));

      isInitialized = true;

    }

    Eigen::Matrix<double, 1, 1> z_measured;
    z_measured(0,0) = msg->height_m;


    if (baro_std <= 0)
    {
      ROS_FATAL_STREAM("Invalid baro_std: " << baro_std);
      assert(0);
    }
    Eigen::Matrix<double, 1, 1> R;
    R(0,0) = baro_std*baro_std;


    // Perform the correction
    filt.measurementType = ImuVo::HEIGHT;
    success = filt.correctionQuaternionWrapper(z_measured, R);
    return success;
  }

  void extrapolateBarometricForward(ImuVo& filt)
  {
#ifdef BARO_OFFSET

    if (baro_drift_std <= 0)
    {
      ROS_FATAL_STREAM("Invalid baro_drift_std: " << baro_drift_std);
      assert(0);
    }
    filt.Q(NOISE_BARO_OFFSET,NOISE_BARO_OFFSET) = baro_drift_std*baro_drift_std;
#endif
  }

};

}


#endif /* SENSOR_WEATHER_BOARD_H_ */
