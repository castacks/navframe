/*
 * sensor_gps.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#ifndef SENSOR_GPS_H_
#define SENSOR_GPS_H_

#include <navframe/sensor_generic.h>
#include <nav_msgs/Odometry.h>

namespace ca
{

class SensorGps : public SensorGeneric
{

public:

  int gps_correction_dof;
  double maxGpsStdev;
  bool hasFailed;
  boost::shared_ptr<diagnostic_updater::TopicDiagnostic> gpsDiag;


  SensorGps()
  {
    reset();
  };

  void reset(void)
  {
    isInitialized = false;
    gps_correction_dof = -1;
    maxGpsStdev = -1;
    hasFailed = true;

  }

  bool storeSensorMsg(filtInst &ss, const nav_msgs::Odometry::ConstPtr& msg, bool hasNewFrame)
  {
    ss.gpsMsg = msg;
    isAlive = true;
    if(gpsDiag) gpsDiag->tick(msg->header.stamp);
    return true;
  }

  bool processSensorMsg(const nav_msgs::Odometry::ConstPtr& msg, ImuVo& filt)
  {
    if (maxGpsStdev <= 0)
    {
      ROS_FATAL_STREAM("Bad parameter for maxGpsStdev: " << maxGpsStdev);
      assert(0);
    }

    // Hard threshold for GPS error by checking variance on GPS x position
    if (msg->pose.covariance[0] > maxGpsStdev*maxGpsStdev || msg->pose.covariance[0] < 0)
    {
      ROS_INFO_STREAM_THROTTLE(1, "GPS measurement rejected. Position std dev: " << sqrt(msg->pose.covariance[0]));
      filt.localFrameValid = false;
      hasFailed = true;
      return false;
    }
    else
      ROS_INFO_STREAM_THROTTLE(1, "GPS position std dev: " << sqrt(msg->pose.covariance[0]));

#ifdef DRIFT
//    if (filt.setNewGpsRef || filt.P(0,0) > 9)  // TODO: Magic number. STDEV > 3 meters. Fairly arbitrary
//    {
//      ROS_INFO_STREAM("Dropping local frame during gps process");
//      filt.setNewGpsRef = false;
//      filt.dropLocalFrame();
//    }

    if (!filt.localFrameValid)
    {
      ROS_INFO_STREAM_THROTTLE(1, "Invalid local frame");
      return false;
    }
#endif


    bool gpsCorrectionSuccess = false;

    switch(gps_correction_dof)
    {

    case 2:
    {
      // GPS is only being used to correct X and Y
      Eigen::Vector2d z_measured;
      z_measured.segment<2>(0) = (ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position)).segment<2>(0);
//      Eigen::Matrix<double, 2, 1> z_measured;
//      z_measured.segment<2>(0) = (CA::msgc(msg->pose.pose.position)).segment<2>(0);

      Eigen::Matrix<double, 6, 6> Rfull;
      Eigen::Matrix<double, 2, 2> R;

      imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, Rfull);
      R.block<2,2>(0,0) = Rfull.block<2,2>(0,0);

      filt.measurementType = ImuVo::GPS_2DOF;
      gpsCorrectionSuccess = filt.correctionQuaternionWrapper(z_measured, R);
    }
    break;

    case 3:
    {
      // GPS is only being used to correct XYZ
      Eigen::Matrix<double, 3, 1> z_measured;
      z_measured = (ca::point_cast<Eigen::Vector3d >(msg->pose.pose.position));
//      z_measured.segment<3>(0) = (CA::msgc(msg->pose.pose.position)).segment<3>(0);

      Eigen::Matrix<double, 6, 6> Rfull;
      Eigen::Matrix<double, 3, 3> R;

      imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, Rfull);
      R.block<3,3>(0,0) = Rfull.block<3,3>(0,0);

      filt.measurementType = ImuVo::GPS_3DOF;
      gpsCorrectionSuccess = filt.correctionQuaternionWrapper(z_measured, R);

    }
    break;

    case 4:
    {
      // Correct using global X and Y position and velocity
      Eigen::Matrix<double, 4, 1> z_measured;
      z_measured.segment<2>(0) = (ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position)).segment<2>(0);
      z_measured.segment<2>(2) = (ca::point_cast<Eigen::Vector3d>(msg->twist.twist.linear)).segment<2>(0);
//      z_measured.segment<2>(0) = (CA::msgc(msg->pose.pose.position)).segment<2>(0);
//      z_measured.segment<2>(2) = (CA::msgc(msg->twist.twist.linear)).segment<2>(0);

      Eigen::Matrix<double, 6, 6> Rfull;
      Eigen::Matrix<double, 4, 4> R;

      imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, Rfull);
      R.block<2,2>(0,0) = Rfull.block<2,2>(0,0);

      imu_filter::message_helper::matrixPtrToEigen(msg->twist.covariance, Rfull);
      R.block<2,2>(2,2) = Rfull.block<2,2>(0,0);

      filt.measurementType = ImuVo::GPS_VEL_4DOF;
      gpsCorrectionSuccess = filt.correctionQuaternionWrapper(z_measured, R);

    }
    break;

    case 5:
    {
      // Correct using global XYZ position and XY velocity

      Eigen::Matrix<double, 5, 1> z_measured;
      Eigen::Matrix<double, 5, 5> R;

      z_measured.segment<3>(0) = (ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position));
      z_measured.segment<2>(3) = (ca::point_cast<Eigen::Vector3d>(msg->twist.twist.linear)).segment<2>(0);
//      z_measured.segment<3>(0) = (CA::msgc(msg->pose.pose.position)).segment<3>(0);
//      z_measured.segment<2>(3) = (CA::msgc(msg->twist.twist.linear)).segment<2>(0);

      Eigen::Matrix<double, 6, 6> Rfull;

      imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, Rfull);
      R.block<3,3>(0,0) = Rfull.block<3,3>(0,0);

      imu_filter::message_helper::matrixPtrToEigen(msg->twist.covariance, Rfull);
      R.block<2,2>(3,3) = Rfull.block<2,2>(0,0);

      filt.measurementType = ImuVo::GPS_VEL_5DOF;
      gpsCorrectionSuccess = filt.correctionQuaternionWrapper(z_measured, R);

    }
    break;

    case 6:
    {
      // Correct using global XYZ position and velocity

      Eigen::Matrix<double, 6, 1> z_measured;
      z_measured.segment<3>(0) = (ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position));
      z_measured.segment<3>(3) = (ca::point_cast<Eigen::Vector3d>(msg->twist.twist.linear));
//      z_measured.segment<3>(0) = (CA::msgc(msg->pose.pose.position)).segment<3>(0);
//      z_measured.segment<3>(3) = (CA::msgc(msg->twist.twist.linear)).segment<3>(0);

      Eigen::Matrix<double, 6, 6> Rfull;
      Eigen::Matrix<double, 6, 6> R;

      imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, Rfull);
      R.block<3,3>(0,0) = Rfull.block<3,3>(0,0);

      imu_filter::message_helper::matrixPtrToEigen(msg->twist.covariance, Rfull);
      R.block<3,3>(3,3) = Rfull.block<3,3>(0,0);

      filt.measurementType = ImuVo::GPS_VEL_6DOF;
      gpsCorrectionSuccess = filt.correctionQuaternionWrapper(z_measured, R);
    }
    break;
    case 7:
    {
      // Correct using global XYZ position and yaw

      Eigen::Matrix<double, 6, 1> z_measured;
      z_measured.segment<3>(0) = (ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position));
      z_measured.segment<3>(3) = filt.quat2rotvec(ca::rot_cast<Eigen::Quaterniond>(msg->pose.pose.orientation));
//      z_measured.segment<3>(0) = (CA::msgc(msg->pose.pose.position)).segment<3>(0);
//      z_measured.segment<3>(3) = filt.quat2rotvec(CA::msgc(msg->pose.pose.orientation));

      Eigen::Matrix<double, 6, 6> Rfull;
      Eigen::Matrix<double, 6, 6> R;

      imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, Rfull);
      R.block<3,3>(0,0) = Rfull.block<3,3>(0,0);

      imu_filter::message_helper::matrixPtrToEigen(msg->twist.covariance, Rfull);
      R.block<3,3>(3,3) = Rfull.block<3,3>(0,0);

      filt.measurementType = ImuVo::GPS_VEL_6DOF;
      gpsCorrectionSuccess = filt.correctionQuaternionWrapper(z_measured, R);

    }
    break;

    default:
      ROS_FATAL_STREAM("Invalid GPS correction DOF type: " << gps_correction_dof);
      break;
    }

    if(!gpsCorrectionSuccess)
    {
      ROS_ERROR_STREAM("gpsHasFailed during ukf correction");
      hasFailed = true;
    }
    return gpsCorrectionSuccess;
  }


};

}

#endif /* SENSOR_GPS_H_ */
