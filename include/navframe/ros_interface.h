/*
 * ros_interface.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#ifndef ROS_INTERFACE_H_
#define ROS_INTERFACE_H_

#include <ukf/ukf.h>
#include <ukf/State.h>
#include <navframe/navframe.h>
#include <navframe/filter_instance.h>

#include <navframe/sensor_weather_board.h>
#include <navframe/sensor_fc_debug.h>
#include <navframe/sensor_visual_odometry.h>
#include <navframe/sensor_imu.h>
#include <navframe/sensor_gps.h>
#include <navframe/sensor_ground_plane_height.h>

#include <ros/ros.h>
#include <ca_common/BoolStamped.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <mikrokopter/FcDebug.h>
#include <spk_weather_board/AirDensityHeight.h>
#include <ca_common/GroundPlaneHRP.h>
#include <tf_utils/tf_utils.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <diagnostic_updater/diagnostic_updater.h>
#include "diagnostic_msgs/SelfTest.h"
#include "self_test/self_test.h"
#include <diagnostic_updater/publisher.h>

#include <Eigen/Eigen>
#include <string>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>

#include <ukf/message_helper.h>
#include <ca_common/math.h>
#include <ca_common/transforms.h>
#include <riverine_watchdog/watchdog.h>
#include <diagnostic_status_refiner/diagnostic_status_refiner.h>



class NodeImuVo {

public:

  NodeImuVo();
  ~NodeImuVo();

  CA::PetWatchdog pet;
  ca::DiagStatusRefiner* refiner;
  diagnostic_updater::Updater updater;
  self_test::TestRunner self_test;
  
//  diagnostic_updater resources
  boost::shared_ptr<diagnostic_updater::Updater> updater_;
  double imu_freq,vo_freq,gps_freq,baro_freq,hrp_freq,max_velocity;
  std::string error_msg;
  
  ros::NodeHandle n_, np_;
  ros::Subscriber initSub_;
  ros::Subscriber reset_sub;
  ros::Publisher 	worldToVehicleOdomPub_, imuToCamOdomPub_, statePublisher, correctPosePub, driftPub;

  message_filters::TimeSynchronizer<nav_msgs::Odometry, ca_common::BoolStamped>* sync;
  message_filters::Subscriber<nav_msgs::Odometry> vo_sub;
  message_filters::Subscriber<ca_common::BoolStamped> newFrameSub;

  ImuVo filt;
  std::string camera_frame_id_, imu_frame_id_, vehicle_frame_id_, reference_frame_, gps_frame_, antenna_frame_id_;
  double alpha;
  double beta;
  double gyro_bias_std, accel_bias_std;
  double mostRecentTime;
  bool publishPrevStates;
  bool useBaro, useWeatherboardBaro, useGPS, useVisualOdometry, useGroundPlane;
  double storageBuffer_s;
  double gravityMagnitude;
  double watchdogFreq;
  bool useCovarianceScaling, useVelocityInflation, useErvInflation;
  Eigen::Vector3d latestGps;

//  bool filterInitialized;
  tf::TransformListener* listener_;
  ros::Timer wdt;

  Eigen::Isometry3d Tiv;
  tf::StampedTransform ref_gps_tf_;
  Eigen::Vector3d initialPosition;

  std::multimap<double, filtInst> filtList;

  ca::SensorWeatherBoard sensorWeatherBd;
  ca::SensorFcDebug sensorFcDebug;
  ca::SensorVisualOdometry sensorVisualOdometry;
  ca::SensorImu sensorImu;
  ca::SensorGps sensorGps;
  ca::SensorGroundHeight sensorAgl;

  void watchdogCallback(const ros::TimerEvent& event);
  void diagnosticUpdateCallback(const ros::TimerEvent& event);

  template <class msgT> void templateCallback(msgT msg);
  void voCallback(const nav_msgs::Odometry::ConstPtr& msg, const ca_common::BoolStamped::ConstPtr& newFrameMsg);

  template <class msgT> std::multimap<double, filtInst>::iterator spliceInBuffer(msgT msg, bool hasNewFrame = false);

  bool storeSensorMsg(filtInst &ss, const sensor_msgs::Imu::ConstPtr& msg, bool hasNewFrame);
  bool storeSensorMsg(filtInst &ss, const mikrokopter::FcDebug::ConstPtr& msg, bool hasNewFrame);
  bool storeSensorMsg(filtInst &ss, const spk_weather_board::AirDensityHeight::ConstPtr& msg, bool hasNewFrame);
  bool storeSensorMsg(filtInst &ss, const nav_msgs::Odometry::ConstPtr& msg, bool hasNewFrame);
  bool storeSensorMsg(filtInst &ss, const ca_common::GroundPlaneHRP::ConstPtr& msg, bool hasNewFrame);

//  template <class msgT> void getTransforms(msgT msg);

  void rerunSensorBuffer(std::multimap<double,filtInst>::iterator it);

  void resetFilter(void);
  void loadParameters(void);
  void subscribeInputTopics(void);


  void initializeFilter(const nav_msgs::Odometry::ConstPtr& msg);
  void findImuCameraTrans(const nav_msgs::Odometry::ConstPtr& msg);
  void findImuVehicleTrans(const nav_msgs::Odometry::ConstPtr& msg);
  bool findStaticTransformation(const std::string &src, const std::string &dst, Eigen::Isometry3d &T_src_dst);

  void resetCallback(const std_msgs::Empty::ConstPtr &msg);

  void cloneLocalFrameCovar(void);
  void dropLocalFrame(void);
  Eigen::MatrixXd transferCovar(int src, int dst, Eigen::MatrixXd &mat);


  void publishOdom(ros::Time msgTime);

  bool transformOdometryMsg(const std::string& target_frame, const nav_msgs::Odometry& odom_in, nav_msgs::Odometry& odom_out, tf::TransformListener* listener);

  //Diagnostic methods
  boost::shared_ptr<diagnostic_updater::TopicDiagnostic> configFrequencyTopicDiagnostic(const std::string& name, double * target);
  void errorDiagnostics(diagnostic_updater::DiagnosticStatusWrapper& status);

};

#endif /* ROS_INTERFACE_H_ */
