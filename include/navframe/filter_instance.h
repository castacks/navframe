/*
 * filter_instance.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#ifndef FILTER_INSTANCE_H_
#define FILTER_INSTANCE_H_

#include <navframe/navframe.h>

#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <mikrokopter/FcDebug.h>
#include <spk_weather_board/AirDensityHeight.h>
#include <ca_common/GroundPlaneHRP.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
class filtInst
{
public:
  ImuVo pose;

  sensor_msgs::Imu::ConstPtr imuMsg;
  nav_msgs::Odometry::ConstPtr voMsg;
  nav_msgs::Odometry::ConstPtr goMsg; //ground3DOF odometry
  mikrokopter::FcDebug::ConstPtr fcMsg;
  spk_weather_board::AirDensityHeight::ConstPtr heightMsg;
  ca_common::GroundPlaneHRP::ConstPtr gpHeightMsg; //ground plane height odometry
  nav_msgs::Odometry::ConstPtr gpsMsg;
  bool newFrame;

  filtInst(ImuVo filtIn):
    pose(filtIn),
    newFrame(false)
  {
  }

};


#endif /* FILTER_INSTANCE_H_ */
