/*
 * sensor_imu.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#ifndef SENSOR_IMU_H_
#define SENSOR_IMU_H_


#include <navframe/sensor_generic.h>
#include <sensor_msgs/Imu.h>


namespace ca
{

class SensorImu : public SensorGeneric
{

public:
  bool useImuMsgParams; // TODO Take care of the case when we do want to use the parameters from the IMU msg
  double accel_std_dev;
  double gyro_std_dev;
  double accel_bias_drive_std;
  double gyro_bias_drive_std;
  double accel_bias_tc;
  double gyro_bias_tc;
  boost::shared_ptr<diagnostic_updater::TopicDiagnostic> imuDiag;

  SensorImu() : useImuMsgParams(false)
  {
    reset();
  };

  void reset(void)
  {
    isInitialized = true;
    accel_std_dev = -1;
    gyro_std_dev = -1;
    accel_bias_drive_std = -1;
    gyro_bias_drive_std = -1;
    accel_bias_tc = -1;
    gyro_bias_tc = -1;

  }


  bool storeSensorMsg(filtInst &ss, const sensor_msgs::Imu::ConstPtr& msg, bool hasNewFrame)
  {
    ss.imuMsg = msg;
    isAlive = true;
    if(imuDiag) imuDiag->tick(msg->header.stamp);
    return true;
  }


  void processSensorMsg(const sensor_msgs::Imu::ConstPtr& msg, ImuVo& filt)
  {
    // Give the incoming IMU data to the filter
    imu_filter::message_helper::linearAccelerationFromImuMsg(msg, filt.accelMeasurement);
    imu_filter::message_helper::angularRateFromImuMsg(msg, filt.gyroMeasurement);
//    filt.accelMeasurement.y()=-filt.accelMeasurement.y();
//    filt.accelMeasurement.z()=-filt.accelMeasurement.z();
//    filt.gyroMeasurement.y()=-filt.gyroMeasurement.y();
//    filt.gyroMeasurement.z()=-filt.gyroMeasurement.z();
  }

  void extrapolateImuForward(ImuVo& filt)
  {
    checkValidParameters();

    if (filt.dt > 0)
    {
      // Make a prediction update using the previous IMU so we predict up until the current time
      filt.n = Eigen::VectorXd::Zero(LENGTH_NOISE_VECTOR);
      filt.Q = Eigen::MatrixXd::Zero(LENGTH_NOISE_VECTOR, LENGTH_NOISE_VECTOR);

      filt.Q.block<3,3>(NOISE_ACCEL,NOISE_ACCEL) = accel_std_dev*accel_std_dev*Eigen::Matrix3d::Identity();
      filt.Q.block<3,3>(NOISE_GYRO,NOISE_GYRO) = gyro_std_dev*gyro_std_dev*Eigen::Matrix3d::Identity();
#ifdef NOISE_BA
      filt.Q.block<3,3>(NOISE_BA,NOISE_BA) = accel_bias_drive_std*accel_bias_drive_std * Eigen::Matrix3d::Identity();
#endif
#ifdef NOISE_BG
      filt.Q.block<3,3>(NOISE_BG,NOISE_BG) = gyro_bias_drive_std*gyro_bias_drive_std * Eigen::Matrix3d::Identity();
#endif
      filt.accel_bias_tc = accel_bias_tc;
      filt.gyro_bias_tc = gyro_bias_tc;

    }
    else
    {
      ROS_FATAL_STREAM("Trying to integrate the IMU backwards in time!");
    }
  }

  void checkValidParameters(void)
  {
    if (accel_std_dev <= 0)
    {
      ROS_FATAL_STREAM("Invalid accel_std_dev: " << accel_std_dev);
      assert(0);
    }

    if (gyro_std_dev <= 0)
    {
      ROS_FATAL_STREAM("Invalid gyro_std_dev: " << gyro_std_dev);
      assert(0);
    }

    if (accel_bias_drive_std <= 0)
    {
      ROS_FATAL_STREAM("Invalid accel_bias_drive_std: " << accel_bias_drive_std);
      assert(0);
    }

    if (gyro_bias_drive_std <= 0)
    {
      ROS_FATAL_STREAM("Invalid gyro_bias_drive_std: " << gyro_bias_drive_std);
      assert(0);
    }

    if (accel_bias_tc <= 0)
    {
      ROS_FATAL_STREAM("Invalid accel_bias_tc: " << accel_bias_tc);
      assert(0);
    }

    if (gyro_bias_tc <= 0)
    {
      ROS_FATAL_STREAM("Invalid gyro_bias_tc: " << gyro_bias_tc);
      assert(0);
    }
  }


};

}

#endif /* SENSOR_IMU_H_ */
