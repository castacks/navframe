/*
 * sensor_visual_odometry.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#ifndef SENSOR_VISUAL_ODOMETRY_H_
#define SENSOR_VISUAL_ODOMETRY_H_

#include <navframe/sensor_generic.h>

#include <nav_msgs/Odometry.h>

namespace ca
{

class SensorVisualOdometry : public SensorGeneric
{

public:

  int vo_correction_dof;
  bool useVoMsgParams;
  bool useKeyFrames;
  boost::shared_ptr<diagnostic_updater::TopicDiagnostic> voDiag;


  SensorVisualOdometry()
  {
    reset();
  };

  void reset(void)
  {
    isInitialized = false;
    vo_correction_dof = -1;
    useVoMsgParams = true;
    useKeyFrames = false;
  }


  bool storeSensorMsg(filtInst &ss, const nav_msgs::Odometry::ConstPtr& msg, bool hasNewFrame)
  {
    ss.voMsg = msg;
    isAlive = true;
    if(voDiag) voDiag->tick(msg->header.stamp);
    return true;
  }

  bool processSensorMsg(const nav_msgs::Odometry::ConstPtr& msg, bool newFrame, ImuVo& filt)
  {
    bool success(false);
    if (!isInitialized)
    {
      std::cout << "First VO callback!" << std::endl;
      isInitialized = true;
    }
    else
    {
      // Check for visual odometry failure:
      // 1. Zero motion reported
      // 2. Zero or negative covariance
      filt.invalidVO = (ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position).array().abs() <= 1e-9).all() || msg->pose.covariance[0] < 0;

      if (filt.invalidVO)
      {
        ROS_WARN_THROTTLE(0.5, "invalidVO");
        ROS_DEBUG("invalidVO");

      }

      if (( (msg->header.stamp.toSec() - filt.lastVO) <= 0.15) &&  !filt.invalidVO)
      {
        // Extract the VO measure to compare against the predicted measurement z
        switch (vo_correction_dof)
        {
        case 9:
        {
          filt.measurementType = ImuVo::VO_POS_VEL;

          Eigen::Matrix<double, 9, 1> z_measured;
          z_measured.segment<3>(0) = ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position);
          z_measured.segment<3>(3) = filt.quat2rotvec(ca::rot_cast<Eigen::Quaterniond>(msg->pose.pose.orientation));
          z_measured.segment<3>(6) = ca::point_cast<Eigen::Vector3d>(msg->twist.twist.linear);

          // Taken from geometry_msgs/PoseWithCovariance Message:
          // # Row-major representation of the 6x6 covariance matrix
          // # The orientation parameters use a fixed-axis representation.
          // # In order, the parameters are:
          // # (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
          Eigen::Matrix<double, 6, 6> Rfull = Eigen::Matrix<double, 6, 6>::Zero();
          Eigen::Matrix<double, 9, 9> R = Eigen::Matrix<double, 9, 9>::Zero();

          if (useVoMsgParams)
          {
            imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, Rfull);
            R.block<6,6>(0,0) = Rfull;

            imu_filter::message_helper::matrixPtrToEigen(msg->twist.covariance, Rfull);
            //R.block<3,3>(6,6) = Rfull.block<3,3>(0,0);
            R.block<3,3>(6,6) =  0.1*Eigen::Matrix3d::Identity();
            //std::cout << R << std::endl;
          }
          else
          {
            R.block<3,3>(0,0) =  0.01*Eigen::Matrix3d::Identity(); // Overwrite the measurement covariance values. As seen in libviso2
            R.block<3,3>(3,3) =  0.001*Eigen::Matrix3d::Identity();
            R.block<3,3>(6,6) =  0.1*Eigen::Matrix3d::Identity();
          }
          //ROS_INFO_STREAM("case 6"<< R);
          success=filt.correctionQuaternionWrapper(z_measured, R);
        }
        break;

        case 6:
        {
          filt.measurementType = ImuVo::VO;
          Eigen::Matrix<double, 6, 1> z_measured;
          z_measured.segment<3>(0) = ca::point_cast<Eigen::Vector3d>(msg->pose.pose.position);
          z_measured.segment<3>(3) = filt.quat2rotvec(ca::rot_cast<Eigen::Quaterniond>(msg->pose.pose.orientation));

          // Taken from geometry_msgs/PoseWithCovariance Message:
          // # Row-major representation of the 6x6 covariance matrix
          // # The orientation parameters use a fixed-axis representation.
          // # In order, the parameters are:
          // # (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
          Eigen::Matrix<double, 6, 6> R = Eigen::Matrix<double, 6, 6>::Zero();

          if (useVoMsgParams)
          {
            imu_filter::message_helper::matrixPtrToEigen(msg->pose.covariance, R);
          }
          else
          {
            R.block<3,3>(0,0) =  0.01*Eigen::Matrix3d::Identity(); // Overwrite the measurement covariance values. As seen in libviso2
            R.block<3,3>(3,3) =  0.001*Eigen::Matrix3d::Identity();
          }
          // Perform the correction
          success=filt.correctionQuaternionWrapper(z_measured, R);

        }
        break;

        default:
          ROS_FATAL_STREAM("Invalid VO correction type: " << vo_correction_dof);
          return success;
          break;

        } // end switch statement

      } // end excute on validVO
    } // end excute on initializedVO


    // Dropping the local frame needs to come BEFORE reseting the delayed state
    // Why?
    // dropLocalFrame needs there to be uncertainty in the position estimation to push into the drift uncertainty
    // but in the act of dropping the local frame, the delayed state becomes invalid. So dropping a local frame can
    // only happen immediately after a VO measurement update but before reseting the delayed state.
  #ifdef DRIFT
    if (filt.setNewGpsRef || filt.P(0,0) > 3 || filt.P(1,1) > 3)  // TODO: Magic number. Fairly arbitrary
    {
      filt.setNewGpsRef = false;
      filt.dropLocalFrame();
    }
  #endif


    if (newFrame || !useKeyFrames)
    {
      filt.resetDelayedState();
    }

    // Only used for the absolute VO measurement model
    //    filt.savePrevT();
    return success;
  } // end processVoMsg


};

}

#endif /* SENSOR_VISUAL_ODOMETRY_H_ */
