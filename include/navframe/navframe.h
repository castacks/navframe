/*
 * navframe.h
 *
 */

// z: Measurement vector
//     cam_pos_curr2prev
//    cam_mrp_curr2prev


#ifndef IMUVO_H_
#define IMUVO_H_

#include <ukf/ukf.h>
#include <iostream>
#include <ca_common/math.h>


// State vector defines:

//#define ENABLE_DRIFTING_NAV_FRAME

#ifdef ENABLE_DRIFTING_NAV_FRAME

#define POSITION  0  // Position (m) of the IMU in world NED frame [x y z]'
#define ERV     3  // Error rotation vector for the rotation of the IMU in the world frame
#define VELOCITY  6  // Velocity (m/s) of the IMU in the world frame
#define DRIFT    9
#define BIAS_ACCEL  12  // Acceleration bias (m/s^2)
#define BIAS_GYRO  15  // Gyroscope bias (rad/s)
#define BARO_OFFSET 18
#define LENGTH_ORIG_STATE  19

#else
#define POSITION  0  // Position (m) of the IMU in world NED frame [x y z]'
#define ERV     3  // Error rotation vector for the rotation of the IMU in the world frame
#define VELOCITY  6  // Velocity (m/s) of the IMU in the world frame
#define BIAS_ACCEL  9  // Acceleration bias (m/s^2)
#define BIAS_GYRO  12  // Gyroscope bias (rad/s)
#define BARO_OFFSET 15
#define LENGTH_ORIG_STATE  16
#endif

#define PREV_POS  LENGTH_ORIG_STATE + 0  // Previous position at last VO update of the IMU in the world frame
#define PREV_ERV  LENGTH_ORIG_STATE + 3  // Previous ERV at the last VO update of the IMU in the world frame
#define LENGTH_DELAYED_STATE   6
#define LENGTH_STATE_VECTOR   LENGTH_ORIG_STATE + LENGTH_DELAYED_STATE

// Noise vector parameter defines:
#define NOISE_ACCEL 0
#define NOISE_GYRO  3
#define NOISE_BA  6
#define NOISE_BG  9

#ifdef BARO_OFFSET
  #define NOISE_BARO_OFFSET 12
  #define LENGTH_NOISE_VECTOR 13
#else
  #define LENGTH_NOISE_VECTOR 12
#endif

#define MIN_ANGLE         0.01 * M_PI/180

class ImuVo : public ca_ukf
{
public:


  Eigen::Vector3d accelMeasurement, gyroMeasurement;
  Eigen::Vector3d gravity;
  double accel_bias_tc, gyro_bias_tc;

  double dt;              // Time step in seconds
  bool isInit;            // Is the filter initialized?
  double time;
  double alpha;
  double beta;

  Eigen::Quaterniond q_w_i;      // Current world to IMU quaternion
  Eigen::Quaterniond prev_q_w_i;    // Previous world to IMU quaternion
  Eigen::Isometry3d Tic;        // Isometric transform 4x4 from the IMU to camera frame. Known from calibration.
  Eigen::Isometry3d Tig;        // IMU to GPS antenna transform
  Eigen::Quaterniond sigma_qk1_0;
  bool sigma_qk1_valid;
  bool invalidVO;
  double initTime;

  double lastVO;
  bool localFrameValid;
  bool setNewGpsRef;

  Eigen::Isometry3d abs_prev_Twi;
  Eigen::Isometry2d prevGlobalDrift;
  Eigen::Isometry2d q_N;

  bool propsOn;

  // Measurement types
  enum { VO, VO_VEL, VO_ABS, VO_POS_VEL, GPS_2DOF, GPS_3DOF, GPS_VEL_4DOF, GPS_VEL_5DOF, GPS_VEL_6DOF, HEIGHT, GRAV };
  int measurementType;

  Eigen::Matrix<double, LENGTH_STATE_VECTOR, LENGTH_ORIG_STATE> sT;

  ImuVo(int xSize, int P_Size, int n_Size, int Q_Size): ca_ukf(xSize, P_Size, n_Size, Q_Size)
  {
    isInit = false;
    sT.setZero(LENGTH_STATE_VECTOR, LENGTH_ORIG_STATE);
    sT.block<LENGTH_ORIG_STATE,LENGTH_ORIG_STATE>(0,0) = Eigen::MatrixXd::Identity(LENGTH_ORIG_STATE,LENGTH_ORIG_STATE);
    sT.block<LENGTH_DELAYED_STATE,LENGTH_DELAYED_STATE>(LENGTH_ORIG_STATE,0) = Eigen::MatrixXd::Identity(LENGTH_DELAYED_STATE,LENGTH_DELAYED_STATE);
    //    std::cout << "sT (" << sT.rows() << "x" << sT.cols() << ")\n" << sT << std::endl;
    time = 0;
    sigma_qk1_valid = false;
    sigma_qk1_0.setIdentity();
    propsOn = true;
    invalidVO = false;

    abs_prev_Twi.setIdentity();
    prevGlobalDrift.setIdentity();
    q_N.setIdentity();
    Tic.setIdentity();
    Tig.setIdentity();

    q_w_i.setIdentity();
    prev_q_w_i.setIdentity();
    accelMeasurement.setZero();
    gyroMeasurement.setZero();
    gravity.setZero();
    
    accel_bias_tc = 0.;
    gyro_bias_tc = 0.;
    dt = 0.;
    alpha = 0.;
    beta = 0.;
    initTime = 0.;
    lastVO = 0.;
    localFrameValid = false;
    setNewGpsRef = false;
    measurementType = 0;
  };

  virtual Eigen::VectorXd predictionModel(const Eigen::VectorXd &x);
  virtual Eigen::VectorXd measurementModel(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_VO(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_VO_POS_VEL(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_VO_VEL(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_VO_ABS(const Eigen::VectorXd &x);

  Eigen::VectorXd measurementModel_HEIGHT(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_GPS_2DOF(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_GPS_3DOF(const Eigen::VectorXd &x);

  Eigen::VectorXd measurementModel_GPS_VEL_4DOF(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_GPS_VEL_5DOF(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_GPS_VEL_6DOF(const Eigen::VectorXd &x);
  Eigen::VectorXd measurementModel_GRAV(const Eigen::VectorXd &x);

  bool copyCovar(int src, int dst, Eigen::MatrixXd &mat);
  Eigen::MatrixXd transferCovar(int src, int dst, Eigen::MatrixXd &mat);
  void cloneLocalFrameCovar(void);
  void dropLocalFrame(void);

  bool checkInitialization(void);

#ifdef POSITION
  inline Eigen::Vector3d getPosition(void)           { return x.segment<3>(POSITION); }
  inline void setPosition(Eigen::Vector3d input)         { x.segment<3>(POSITION) = input; }
#endif

#ifdef ERV
  inline Eigen::Vector3d getErvError(void)           { return x.segment<3>(ERV); }
  inline void setErvError(Eigen::Vector3d input)         { x.segment<3>(ERV) = input; }
#endif

#ifdef VELOCITY
  inline Eigen::Vector3d getVelocity(void)           { return x.segment<3>(VELOCITY); }
  inline void setVelocity(Eigen::Vector3d input)         { x.segment<3>(VELOCITY) = input; }
#endif

#ifdef DRIFT
  inline Eigen::Vector3d getDrift(void)           { return x.segment<3>(DRIFT); }
  inline void setDrift(Eigen::Vector3d input)         { x.segment<3>(DRIFT) = input; }
#endif

#ifdef BIAS_ACCEL
  inline Eigen::Vector3d getBiasAccel(void)           { return x.segment<3>(BIAS_ACCEL); }
  inline void setBiasAccel(Eigen::Vector3d input)       { x.segment<3>(BIAS_ACCEL) = input; }
#endif

#ifdef BIAS_GYRO
  inline Eigen::Vector3d getBiasGyro(void)           { return x.segment<3>(BIAS_GYRO); }
  inline void setBiasGyro(Eigen::Vector3d input)         { x.segment<3>(BIAS_GYRO) = input; }
#endif

#ifdef BIAS_GRAV
  inline Eigen::Vector3d getBiasGrav(void)           { return x.segment<3>(BIAS_GRAV); }
  inline void setBiasGrav(Eigen::Vector3d input)         { x.segment<3>(BIAS_GRAV) = input; }
#endif

#ifdef PREV_POS
  inline Eigen::Vector3d getPrevPos(void)           { return x.segment<3>(PREV_POS);   }
  inline void setPrevPos(Eigen::Vector3d input)         { x.segment<3>(PREV_POS)   = input; }
#endif

#ifdef PREV_ERV
  inline Eigen::Vector3d getPrevErv(void)           { return x.segment<3>(PREV_ERV);   }
  inline void setPrevErv(Eigen::Vector3d input)         { x.segment<3>(PREV_ERV)   = input; }
#endif

  void resetDelayedState(void);
  void savePrevT(void);

  Eigen::Quaterniond rotVecToQuat(const Eigen::Vector3d &v);
  Eigen::Vector3d quat2rotvec(Eigen::Quaterniond q);

  template<typename T, size_t a>
  Eigen::MatrixXd bArray2Eigen(const boost::array<T, a> &bArray, int i, int j, int iSteps, int jSteps);

  void predictionQuaternionWrapper(void);
  bool correctionQuaternionWrapper(Eigen::VectorXd z, Eigen::MatrixXd R);


};

#endif /* IMUVO_H_ */
