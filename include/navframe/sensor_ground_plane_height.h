/*
 * sensor_ground_height.h
 *
 *  Created on: Feb 28, 2014
 *      Author: sezal
 * It is replacing the barometer sensor and cannot be used with sensor_weather_board 
 */

#include <navframe/sensor_generic.h>

#ifndef SENSOR_GROUND_HEIGHT_H_
#define SENSOR_GROUND_HEIGHT_H_


namespace ca
{

class SensorGroundHeight : public SensorGeneric
{

public:
  double agl_std;
  double agl_drift_std;
  boost::shared_ptr<diagnostic_updater::TopicDiagnostic> hrpDiag;

  SensorGroundHeight()
  {
    reset();
  };

  void reset(void)
  {
    agl_std = -1;
    agl_drift_std = -1;
  }


  bool storeSensorMsg(filtInst &ss, const ca_common::GroundPlaneHRP::ConstPtr& msg, bool hasNewFrame)
  {
    //ROS_INFO("sensor_ground_plane_height::Height msg received");
    ss.gpHeightMsg = msg;
    isAlive = true;
    if(hrpDiag) hrpDiag->tick(msg->header.stamp);
    return true;
  }

  bool processSensorMsg(const ca_common::GroundPlaneHRP::ConstPtr& msg, ImuVo& filt)
  {
    bool success = false;

    if (!isInitialized || abs(filt.x(BARO_OFFSET)) < 1e-12)
    {
      filt.x(BARO_OFFSET) = msg->height + filt.x(POSITION+2);
      ROS_INFO_STREAM("Initializing agl sensor... ");
      ROS_INFO_STREAM("Ground Height: " << msg->height << " Z: " << filt.x(POSITION+2) << " Offset: " << filt.x(BARO_OFFSET));

      isInitialized = true;

    }
    Eigen::Matrix<double, 1, 1> z_measured;
    z_measured(0,0) = msg->height;

    if (agl_std <= 0)
    {
      ROS_FATAL_STREAM("Invalid agl_std: " << agl_std);
      assert(0);
    }
    Eigen::Matrix<double, 1, 1> R;
    R(0,0) = agl_std*agl_std;


    // Perform the correction
    filt.measurementType = ImuVo::HEIGHT;
    success = filt.correctionQuaternionWrapper(z_measured, R);
    return success;
  }

  void extrapolateAGLForward(ImuVo& filt)
  {
#ifdef BARO_OFFSET

    if (agl_drift_std <= 0)
    {
      ROS_FATAL_STREAM("Invalid agl_drift_std: " << agl_drift_std);
      assert(0);
    }
    filt.Q(NOISE_BARO_OFFSET,NOISE_BARO_OFFSET) = agl_drift_std*agl_drift_std;
#endif
  }

};

}


#endif /* SENSOR_GROUND_HEIGHT_H_ */
