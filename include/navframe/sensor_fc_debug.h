/*
 * sensor_fc_debug.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#ifndef SENSOR_FC_DEBUG_H_
#define SENSOR_FC_DEBUG_H_

#include <navframe/navframe.h>
#include <navframe/filter_instance.h>
#include <navframe/sensor_generic.h>
#include <mikrokopter/FcDebug.h>

namespace ca
{

class SensorFcDebug : public SensorGeneric
{

public:
  double baro_std;
  int startBaro;
  float startHeight;


  SensorFcDebug()
  {
    reset();
  };


  bool storeSensorMsg(filtInst &ss, const mikrokopter::FcDebug::ConstPtr& msg, bool hasNewFrame)
  {
    ss.fcMsg = msg;
    isAlive = true;
    return true;
  }

  void reset(void)
  {
    baro_std = 1;
    startBaro = -1;
    startHeight = 0.;
  }

  void processSensorMsg(const mikrokopter::FcDebug::ConstPtr& msg, ImuVo& filt)
  {
    if (msg->gas > 100)
      filt.propsOn = true;
    else
      filt.propsOn = false;

    if (!isInitialized || startBaro == -1)
    {
      isInitialized = true;

      startBaro = msg->heightValue;
      startHeight = filt.getPosition().coeff(2);

      ROS_INFO_STREAM("Initializing barometer: " << startBaro << " units = " << startHeight << " m." );
    }

    Eigen::Matrix<double, 1, 1> z_measured;
    z_measured(0,0) = -(float)(msg->heightValue - startBaro)/20.43 + startHeight;   // http://mikrokopter.de/ucwiki/en/heightsensor

    Eigen::Matrix<double, 1, 1> R;
    R(0,0) = baro_std*baro_std;

    // Perform the correction
    filt.measurementType = ImuVo::HEIGHT;
    filt.correctionQuaternionWrapper(z_measured, R);

  }


};

}

#endif /* SENSOR_FC_DEBUG_H_ */
