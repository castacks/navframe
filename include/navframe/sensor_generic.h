/*
 * sensor_generic.h
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#ifndef SENSOR_GENERIC_H_
#define SENSOR_GENERIC_H_

#include <ros/ros.h>

#include <navframe/navframe.h>
#include <navframe/filter_instance.h>
#include <ukf/message_helper.h>
#include <geom_cast/geom_cast.hpp>

namespace ca
{

class SensorGeneric
{

public:
  ros::Subscriber subscriber;
  bool isAlive;
  bool isInitialized;

  SensorGeneric() :
  isAlive(false),
  isInitialized(false)
  {  };

  virtual void reset(void) = 0;

};

} // end namespace


#endif /* SENSOR_GENERIC_H_ */
