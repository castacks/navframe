/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Empty.h>
#include <Eigen/Eigen>
#include <ca_common/math.h>
#include <tf_conversions/tf_eigen.h>
#include <geom_cast/geom_cast.hpp>

using namespace std;

ros::Publisher odomPub;
ros::Subscriber sub_nav, sub_odom;

string localframe, absoluteframe;

nav_msgs::Odometry::ConstPtr currentNav;
tf::TransformListener* listener_;

void navCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
	absoluteframe = msg->header.frame_id;
	currentNav = msg;
}

void timerCallback(const ros::TimerEvent& event)
{
	if (!currentNav)
		return;

	ros::Time now = ros::Time::now();
	//	Eigen::Isometry3d T_local_nav;

	Eigen::Affine3d aT_local_nav;
	bool validTF = listener_->waitForTransform(localframe, currentNav->child_frame_id, now, ros::Duration(1.0) );
	if (validTF)
	{
		tf::StampedTransform Tf_l_n;
		listener_->lookupTransform(localframe , currentNav->child_frame_id , now, Tf_l_n);
		tf::transformTFToEigen(Tf_l_n, aT_local_nav);
		//		T_local_nav = aT_local_nav.matrix();

		ROS_INFO_STREAM(ros::this_node::getName() << ": Found " << localframe << " to " << currentNav->child_frame_id << " transform:\n" << aT_local_nav.matrix());
	}
	else
	{
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Cannot find transform from " << localframe << " to " << currentNav->child_frame_id << ".");
	}



	double yaw;

	yaw = CA::quatToEuler(Eigen::Quaterniond(aT_local_nav.linear()))(2);
	Eigen::Isometry3d T_local_nav(CA::eulerToQuat(CA::Vector3D(0,0,yaw)));
	T_local_nav.translation() = aT_local_nav.translation();


	yaw = CA::quatToEuler(CA::msgc(currentNav->pose.pose.orientation))(2);
	Eigen::Isometry3d T_absolute_nav(CA::eulerToQuat(CA::Vector3D(0,0,yaw)));
	T_absolute_nav.translation() = CA::msgc(currentNav->pose.pose.position);

	// HACK HACK
	//	T_absolute_nav = T_absolute_nav*CA::eulerToQuat(CA::Vector3D(0,0.785,0));

	cout << T_absolute_nav.matrix() << endl;

	Eigen::Isometry3d T_local_absolute = T_local_nav * T_absolute_nav.inverse();
	Eigen::Isometry3d T_absolute_local = T_local_absolute.inverse();


	nav_msgs::Odometry odom_world_ned;

	odom_world_ned.header.stamp = currentNav->header.stamp;
	odom_world_ned.header.frame_id = localframe;
	odom_world_ned.child_frame_id = absoluteframe;
	odom_world_ned.pose.pose = CA::msgc(T_local_absolute);

	//	odom_world_ned.header.frame_id = absoluteframe;
	//	odom_world_ned.child_frame_id = localframe;
	//	odom_world_ned.pose.pose = CA::msgc(T_absolute_local);

	odomPub.publish(odom_world_ned);

	currentNav.reset();
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "drift_estimator");
	ros::NodeHandle n;
	ros::NodeHandle np("~");

	double updateFreq;
	np.param<double>("updateFreq", updateFreq, 1.0);
	np.param<string>("localframe", localframe, "/world");

	ros::Timer timer = n.createTimer(ros::Duration(1.0/updateFreq), timerCallback);

	sub_nav = n.subscribe("nav", 10, navCallback);
	odomPub = n.advertise<nav_msgs::Odometry>("drift", 10);

	listener_ = new tf::TransformListener();

	ros::spin();

	return 0;
}
