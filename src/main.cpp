/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <navframe/ros_interface.h>

int main(int argc, char **argv)  {

  ros::init(argc, argv, "navframe_node");

  NodeImuVo nc;

  ros::spin();

  // Do pre-shutdown tasks

  ros::shutdown();

  return 0;
}



