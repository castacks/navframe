/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/*
 * least_squares_drift_estimator.cpp
 *
 *  Created on: Apr 18, 2013
 *      Author: achamber
 *
 *      Resources:
 *      http://nghiaho.com/?page_id=671
 *      http://igl.ethz.ch/projects/ARAP/svd_rot.pdf
 *
 */

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <nav_msgs/Odometry.h>
#include <ca_common/math.h>
#include <geom_cast/geom_cast.hpp>
#include <Eigen/Eigen>
#include <deque>

using namespace message_filters;
using namespace std;

ros::Publisher odomPub;


deque<nav_msgs::OdometryConstPtr> deque1;
deque<nav_msgs::OdometryConstPtr> deque2;

int bufferSize_;
double distIncr;

void solveLS(void)
{

	Eigen::Vector2d c1 = Eigen::Vector2d::Zero();
	Eigen::Vector2d c2 = Eigen::Vector2d::Zero();

	deque<nav_msgs::OdometryConstPtr>::iterator it1 = deque1.begin();
	deque<nav_msgs::OdometryConstPtr>::iterator it2 = deque2.begin();

	while (it1 != deque1.end())
	{
		c1(0) += (*it1)->pose.pose.position.x;
		c1(1) += (*it1)->pose.pose.position.y;

		c2(0) += (*it2)->pose.pose.position.x;
		c2(1) += (*it2)->pose.pose.position.y;

		++it1;
		++it2;
	}

	int n = deque1.size();

	c1 /= n;
	c2 /= n;

	Eigen::Matrix2d H = Eigen::Matrix2d::Zero();

	it1 = deque1.begin();
	it2 = deque2.begin();

	while (it1 != deque1.end() && it2 != deque2.end())
	{
		Eigen::Vector2d a((*it1)->pose.pose.position.x, (*it1)->pose.pose.position.y);
		Eigen::Vector2d b((*it2)->pose.pose.position.x, (*it2)->pose.pose.position.y);

		H += (a-c1)*(b-c2).transpose();

		++it1;
		++it2;
	}


	Eigen::JacobiSVD<Eigen::Matrix2d> svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);


	Eigen::Matrix2d R = svd.matrixV()*svd.matrixU().transpose();


	if (R.determinant() < 0)
	{
		Eigen::Matrix2d I = Eigen::Matrix2d::Identity();
		I(1,1) = -1;

		R = svd.matrixV()*I*svd.matrixU().transpose();

	}

	Eigen::Vector2d t = -R*c1 + c2;

	Eigen::Isometry3d T_local_absolute = Eigen::Isometry3d::Identity();

	T_local_absolute.matrix().block(0,0,2,2) = R;
	T_local_absolute.translation() = Eigen::Vector3d(t(0), t(1), 0);

	//	cout << T_local_absolute.inverse().matrix() << endl;

	// Publish odometry msg
	nav_msgs::Odometry odom_world_ned;

	odom_world_ned.header.stamp = deque1.back()->header.stamp;
	odom_world_ned.header.frame_id = deque2.back()->header.frame_id;
	odom_world_ned.child_frame_id = deque1.back()->header.frame_id;
	Eigen::Vector3d a=T_local_absolute.translation();
  odom_world_ned.pose.pose.position = ca::point_cast<geometry_msgs::Point>(a);
  odom_world_ned.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(Eigen::Quaterniond(T_local_absolute.rotation()));
	odomPub.publish(odom_world_ned);


}

double distXY(const nav_msgs::OdometryConstPtr& odom1, const nav_msgs::OdometryConstPtr& odom2)
{
	double dist = sqrt(pow(odom1->pose.pose.position.x - odom2->pose.pose.position.x, 2) +
			pow(odom1->pose.pose.position.y - odom2->pose.pose.position.y, 2));
	return dist;

}

void callback(const nav_msgs::OdometryConstPtr& odom1, const nav_msgs::OdometryConstPtr& odom2)
{
	if (deque1.size() == 0)
	{
		deque1.push_back(odom1);
		deque2.push_back(odom2);
		return;
	}

	if (distXY(odom1, deque1.back()) > distIncr && distXY(odom2, deque2.back()) > distIncr)
	{
		deque1.push_back(odom1);
		deque2.push_back(odom2);

		if (deque1.size() > bufferSize_)
		{
			deque1.pop_front();
			deque2.pop_front();

			solveLS();
		}
	}
	return;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "least_squares_drift_estimator");

	ros::NodeHandle nh;
	ros::NodeHandle np("~");

	message_filters::Subscriber<nav_msgs::Odometry> odom1_sub(nh, "odom1", 100);
	message_filters::Subscriber<nav_msgs::Odometry> odom2_sub(nh, "odom2", 100);

	typedef sync_policies::ApproximateTime<nav_msgs::Odometry, nav_msgs::Odometry> MySyncPolicy;

	// ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
	Synchronizer<MySyncPolicy> sync(MySyncPolicy(100), odom1_sub, odom2_sub);
	sync.registerCallback(boost::bind(&callback, _1, _2));

	odomPub = nh.advertise<nav_msgs::Odometry>("drift", 10);

	np.param<int>("bufferSize", bufferSize_, 10);
	np.param<double>("distIncr", distIncr, 1.0);


	ros::spin();

	return 0;
}
