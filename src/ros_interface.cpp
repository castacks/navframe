/*
* Copyright (c) 2016 Carnegie Mellon University, Author <achamber>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/*
 * ros_interface.cpp
 *
 *  Created on: Jun 12, 2013
 *      Author: achamber
 */

#include <navframe/ros_interface.h>

NodeImuVo::NodeImuVo():
filt(LENGTH_STATE_VECTOR, LENGTH_STATE_VECTOR, LENGTH_NOISE_VECTOR, LENGTH_NOISE_VECTOR),
self_test(),
refiner(NULL)
{
  np_ = ros::NodeHandle("~");

  listener_ = new tf::TransformListener;

  updater.setHardwareID("navframe");

  if(!pet.setup(np_))
  {
    ROS_FATAL_STREAM("navframe: Was not able to setup watchdog");
  }

  // Create the watchdog timmer
  np_.param<double>("watchdogFreq", watchdogFreq, 0.5); // Freq in Hz
  wdt = n_.createTimer(ros::Duration(1/watchdogFreq), &NodeImuVo::watchdogCallback, this);
//  diag_timer = n_.createTimer(ros::Duration(1/watchdogFreq), &NodeImuVo::diagnosticUpdateCallback, this);

  reset_sub = n_.subscribe("/ukf/reset", 1, &NodeImuVo::resetCallback, this);
  sync = new message_filters::TimeSynchronizer<nav_msgs::Odometry, ca_common::BoolStamped>(100);

  // filterInitialized=false;
  // Advertise output topics
  worldToVehicleOdomPub_ = n_.advertise<nav_msgs::Odometry>("/ukf/imu/odometry", 100);
  statePublisher = n_.advertise<ukf::State>("/ukf/state", 10);
  correctPosePub = n_.advertise<nav_msgs::Odometry>("/ukf/correctedpose", 10);

#ifdef DRIFT
  driftPub = n_.advertise<nav_msgs::Odometry>("/ukf/drift", 10);
#endif

  resetFilter();
  if(imu_freq) sensorImu.imuDiag = configFrequencyTopicDiagnostic("imu frequency", &imu_freq);
  if(vo_freq && useVisualOdometry) sensorVisualOdometry.voDiag = configFrequencyTopicDiagnostic("vo frequency", &vo_freq);
  if(gps_freq && useGPS) sensorGps.gpsDiag = configFrequencyTopicDiagnostic("gps frequency", &gps_freq);
  if(baro_freq && useBaro) sensorWeatherBd.baroDiag = configFrequencyTopicDiagnostic("baro frequency", &baro_freq);
  if(hrp_freq && useGroundPlane) sensorAgl.hrpDiag = configFrequencyTopicDiagnostic("hrp frequency", &hrp_freq);
  updater.add("runtime errors", this, &NodeImuVo::errorDiagnostics);

  return;
}

NodeImuVo::~NodeImuVo() {
  delete listener_;
  delete sync;
  delete refiner;
}
boost::shared_ptr<diagnostic_updater::TopicDiagnostic> NodeImuVo::configFrequencyTopicDiagnostic(
    const std::string& name, double * target) {
  boost::shared_ptr<diagnostic_updater::TopicDiagnostic> diag;
  const double period = 1.0 / *target;  //  for 1000Hz, period is 1e-3
  
  diagnostic_updater::FrequencyStatusParam freqParam(target, target, 0.01, 10);
  diagnostic_updater::TimeStampStatusParam timeParam(0, period *2);
  diag.reset(new diagnostic_updater::TopicDiagnostic(name, 
                                                     updater, 
                                                     freqParam,
                                                     timeParam));
  return diag;
}

void NodeImuVo::errorDiagnostics(diagnostic_updater::DiagnosticStatusWrapper& status){
  // This is to test ....
  refiner->AggregateEntries(status);
//  if (!error_msg.empty()) status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, boost::to_string(ros::Time::now())+" :: "+error_msg);
//  error_msg.clear();
} 


void NodeImuVo::loadParameters(void) {

  /*
   * SENSOR PARAMETERS
   */
  np_.param<int>("gps_correction_dof", sensorGps.gps_correction_dof, 2);
  np_.param<int>("vo_correction_dof", sensorVisualOdometry.vo_correction_dof, 6);

  /*
   * IMU Parameters
   */

  np_.param<bool>("useImuMsgParams", sensorImu.useImuMsgParams, true);
  np_.param<double>("accel_std_dev", sensorImu.accel_std_dev,   0.177);
#ifdef BIAS_ACCEL
  np_.param<double>("accel_bias_std",  accel_bias_std,   0.042);
  np_.param<double>("accel_bias_tc",  sensorImu.accel_bias_tc,   6);

  sensorImu.accel_bias_drive_std = sqrt(2*50/ sensorImu.accel_bias_tc)*accel_bias_std;
  ROS_DEBUG_STREAM("Accel bias drive std: " <<  sensorImu.accel_bias_drive_std);
#endif

  np_.param<double>("gyro_std_dev",  sensorImu.gyro_std_dev,     0.035*M_PI/180);
#ifdef BIAS_GYRO
  np_.param<double>("gyro_bias_std", gyro_bias_std,     0.0025);
  np_.param<double>("gyro_bias_tc",  sensorImu.gyro_bias_tc,     250);

  sensorImu.gyro_bias_drive_std = sqrt(2*50/ sensorImu.gyro_bias_tc)*gyro_bias_std;
  ROS_DEBUG_STREAM("gyro_bias_std: " << gyro_bias_std);
  ROS_DEBUG_STREAM("gyro_bias_drive_std: " << sensorImu.gyro_bias_drive_std);
#endif

  /*
   * Barometric Pressure Parameters
   */
  np_.param<bool>("useBaro", useBaro, true);
  np_.param<bool>("useWeatherboardBaro", useWeatherboardBaro, true);
  np_.param<bool>("useGroundPlane", useGroundPlane, true);
  np_.param<double>("baro_std", sensorWeatherBd.baro_std,   1);
  np_.param<double>("baro_std", sensorFcDebug.baro_std,   1);
  np_.param<double>("baro_std", sensorAgl.agl_std,   1);
#ifdef BARO_OFFSET
  np_.param<double>("baro_drift_std", sensorWeatherBd.baro_drift_std, 0.001);
  np_.param<double>("baro_drift_std", sensorAgl.agl_drift_std, 0.001);
#endif

  /*
   * Visual Odometry Parameters
   */
  np_.param<bool>("useVoMsgParams", sensorVisualOdometry.useVoMsgParams, true);
  np_.param<bool>("useVisualOdometry", useVisualOdometry, true);
  np_.param<bool>("keyFrames", sensorVisualOdometry.useKeyFrames, false);

  /*
   * GPS parameters
   */
  useGPS = false;
  np_.getParamCached("useGPS", useGPS);
  np_.param<double>("maxGpsStdev", sensorGps.maxGpsStdev, 1.5);

  /*
   * Other parameters
   */
  np_.param<double>("gravityMagnitude", gravityMagnitude, 9.81);

  np_.param<bool>("useCovarianceScaling", useCovarianceScaling, false);
  np_.param<bool>("useVelocityInflation", useVelocityInflation, false);
  np_.param<bool>("useErvInflation", useErvInflation, false);

  np_.param<double>("alpha", alpha, 0.6);              // alpha controls the spread of the sigma points about the state mean. It is usually set to a small positive value between 1 and 1e-3
  np_.param<double>("beta", beta, 2);               // setting beta = 2 minimizes the fourth-order error for jointly Gaussian distributions.
  np_.param<double>("storageBuffer_s", storageBuffer_s, 2.0);    // Sensor data buffer length in seconds to store
  np_.param<bool>("publishPrevStates", publishPrevStates, false);  // For debugging

  /*
   * Coordinate frame information
   */
  np_.param<std::string>("imu_frame_id", imu_frame_id_, "/imu");
  np_.param<std::string>("camera_frame_id", camera_frame_id_, "/camera");
  np_.param<std::string>("vehicle_frame_id", vehicle_frame_id_, "/base_frame");
  np_.param<std::string>("antenna_frame_id", antenna_frame_id_, "/gps_antenna");
  np_.param<std::string>("reference_frame", reference_frame_, "/world");

  /*
  * Diagnostic Information
  */
  np_.param<double>("imu_frequency",imu_freq,0.0);
  np_.param<double>("vo_frequency",vo_freq,0.0);
  np_.param<double>("gps_frequency",gps_freq,0.0);
  np_.param<double>("baro_frequency",baro_freq,0.0);
  np_.param<double>("max_velocity",max_velocity,3.0);
}

void NodeImuVo::resetFilter(void) {
  // Clear the buffer of previous sensor measurements
  filtList.clear();
  mostRecentTime = 0;
  filt.localFrameValid = false;

  loadParameters();
  subscribeInputTopics();

  if(refiner) delete refiner;
  refiner = new ca::DiagStatusRefiner("runtime errors");
  return;
}

/*
 * Subscribe to various input topics
 */
void NodeImuVo::subscribeInputTopics(void) {
  ros::TransportHints ros_transport_hints = ros::TransportHints().udp().tcpNoDelay();

  // Initialization msg
  initSub_ = n_.subscribe<nav_msgs::Odometry>("input/imu/initial", 10, &NodeImuVo::initializeFilter, this);

  // IMU msg (required since we are using IMU mechnization)
  sensorImu.subscriber = n_.subscribe<sensor_msgs::Imu>("input/imu", 100, &NodeImuVo::templateCallback, this, ros_transport_hints);

  if (useVisualOdometry) {
    if (sensorVisualOdometry.useKeyFrames) {
      vo_sub.subscribe(n_,"input/vo",100);
      newFrameSub.subscribe(n_,"/kfmatcher/hasNewFrame",100);

      sync->connectInput(vo_sub, newFrameSub);
      sync->registerCallback(boost::bind(&NodeImuVo::voCallback, this, _1, _2));

      std::cout << "Registered VO Keyframe TimeSynchronizer" << std::endl;
    }
    else {
      sensorVisualOdometry.subscriber = n_.subscribe<nav_msgs::Odometry>("input/vo", 100, &NodeImuVo::templateCallback, this);  // I don't want to lose any VO messages
    }
  } else {
    sensorVisualOdometry.subscriber.shutdown();
    vo_sub.unsubscribe();
    newFrameSub.unsubscribe();
  }

  if (useBaro) {
    if (useWeatherboardBaro) {
      ROS_INFO("IMUVO: Using spk_weather_board::AirDensityHeight for barometric data");
      sensorWeatherBd.subscriber = n_.subscribe<spk_weather_board::AirDensityHeight>("input/baro", 100, &NodeImuVo::templateCallback, this);
      sensorFcDebug.subscriber.shutdown();
    }
    else {
      ROS_INFO("IMUVO: Using mikrokopter::FcDebug for barometric data");
      sensorFcDebug.subscriber = n_.subscribe<mikrokopter::FcDebug>("input/baro", 100, &NodeImuVo::templateCallback, this);
      sensorWeatherBd.subscriber.shutdown();
    }
  }
  if (useGroundPlane) {
      ROS_INFO("IMUVO: Using ground plane odometry for height data");
      sensorAgl.subscriber = n_.subscribe<ca_common::GroundPlaneHRP>("input/hrp", 100, &NodeImuVo::templateCallback, this);
      sensorFcDebug.subscriber.shutdown();
  }

  if (useGPS) {
    sensorGps.subscriber = n_.subscribe<nav_msgs::Odometry>("input/gps", 100, &NodeImuVo::templateCallback, this, ros_transport_hints);
  } else {
    sensorGps.subscriber.shutdown();
  }

}

void NodeImuVo::resetCallback(const std_msgs::Empty::ConstPtr &msg) {
  resetFilter();
  ROS_INFO_STREAM("IMUVO: Got reset request");
}

void NodeImuVo::initializeFilter(const nav_msgs::Odometry::ConstPtr& msg) {
//if ((ros::Time::now() - msg->header.stamp).toSec() > storageBuffer_s/2)
//{
//  ROS_FATAL_STREAM("navframe: Initialization msg is too old. Age: " << (ros::Time::now() - msg->header.stamp));
//  return;
//}

  nav_msgs::Odometry initOdom = *msg;

  std::cout << "Initializing UKF..." << std::endl;
  std::cout << "gravityMagnitude: " << gravityMagnitude << std::endl;
  filt.gravity << 0, 0, gravityMagnitude;

  if (initOdom.header.frame_id != reference_frame_) {
    ROS_WARN_STREAM(ros::this_node::getName() << ": frame_id of the initialization message is not the same as the state estimation fixed reference frame!");
    ROS_WARN_STREAM(ros::this_node::getName() << ": " << initOdom.header.frame_id << " != " << reference_frame_);
    if (!transformOdometryMsg(reference_frame_, initOdom, initOdom, listener_)){
      return;}
  }
  if (initOdom.child_frame_id != imu_frame_id_) {
    ROS_FATAL_STREAM(ros::this_node::getName() << ": child_frame_id of the initialization message is not the same as the state estimation body frame!");
    ROS_FATAL_STREAM(ros::this_node::getName() << ": " << initOdom.child_frame_id << " != " << imu_frame_id_);
    //assert(0);
  }
  //only find transforms if the respective sensor is being used
  if (useVisualOdometry&&!CA::findTransformation(listener_, imu_frame_id_, camera_frame_id_, msg->header.stamp, ros::Duration(0.5), filt.Tic))
    return;
  if (!CA::findTransformation(listener_, imu_frame_id_, vehicle_frame_id_, msg->header.stamp, ros::Duration(0.5), Tiv))
    return;
  if (useGPS&&!CA::findTransformation(listener_, imu_frame_id_, antenna_frame_id_, msg->header.stamp, ros::Duration(0.5), filt.Tig))
    return;
  // Begin initialization...

  filt.time = initOdom.header.stamp.toSec();

  filt.alpha = alpha;
  filt.beta = beta;

  // Set all the initial values in the state vector
  filt.x.setZero();
#ifdef POSITION
  //  filt.setPosition(Eigen::Vector3d(0, 0, initOdom.pose.pose.position.z));
  //  filt.setPrevPos(filt.getPosition());
#endif
#ifdef ERV
  filt.setErvError(Eigen::Vector3d::Zero());
  filt.setPrevErv(Eigen::Vector3d::Zero());
#endif
#ifdef VELOCITY
  filt.setVelocity(ca::point_cast<Eigen::Vector3d>(initOdom.twist.twist.linear));
#endif
#ifdef DRIFT

  filt.setPosition(Eigen::Vector3d(0, 0, initOdom.pose.pose.position.z));
  filt.setPrevPos(filt.getPosition());

  Eigen::Vector2d driftVector;
  driftVector(0) = initOdom.pose.pose.position.x;
  driftVector(1) = initOdom.pose.pose.position.y;
  filt.prevGlobalDrift.translate(driftVector);

  filt.setDrift(Eigen::Vector3d::Zero());
#else
  filt.setPosition(ca::point_cast<Eigen::Vector3d>(initOdom.pose.pose.position));
  filt.setPrevPos(filt.getPosition());

  //initialPosition.segment<2>(0) = filt.getPosition().segment<2>(0);
#endif

#ifdef BIAS_ACCEL
  filt.setBiasAccel(Eigen::Vector3d::Zero());
  //filt.setBiasAccel(accel_bias_std*Eigen::Vector3d::Ones());  // Start with an error on the bias to demonstrate that it can be corrected
#endif
#ifdef BIAS_GYRO
  filt.setBiasGyro(Eigen::Vector3d::Zero());
  //filt.setBiasGyro(gyro_bias_std*Eigen::Vector3d::Ones()); // Start with an error on the bias to demonstrate that it can be corrected
#endif
#ifdef BIAS_GRAV
  filt.setBiasGrav(Eigen::Vector3d::Zero());
#endif

  filt.q_w_i = ca::rot_cast<Eigen::Quaterniond>(initOdom.pose.pose.orientation);
  filt.prev_q_w_i = filt.q_w_i;

  // Initialize state covariance matrix
  filt.P.setZero();
  Eigen::Matrix<double, 6, 6> poseCovar(6,6);
  Eigen::Matrix<double, 6, 6> twistCovar(6,6);
  imu_filter::message_helper::matrixPtrToEigen(initOdom.pose.covariance, poseCovar);
  imu_filter::message_helper::matrixPtrToEigen(initOdom.twist.covariance, twistCovar);
  filt.P.block<6,6>(0,0) = poseCovar;

  filt.P.block<3,3>(ERV,ERV)= Eigen::Matrix3d::Zero();

  filt.P.block<3,3>(VELOCITY,VELOCITY)   = twistCovar.block<3,3>(0,0);    // Velocity m/s

#ifdef DRIFT
//std::cout<< filt.P << std::endl;
filt.cloneLocalFrameCovar();
//filt.P(DRIFT+2, DRIFT+2) += 90 * M_PI/180;

//std::cout<< "New P:" << std::endl;
//std::cout<< filt.P << std::endl;
#endif

#ifdef BIAS_ACCEL
  filt.P.block<3,3>(BIAS_ACCEL,BIAS_ACCEL)= accel_bias_std * accel_bias_std * Eigen::Matrix3d::Identity();          // Bias Accel
#endif

#ifdef BIAS_GYRO
  filt.P.block<3,3>(BIAS_GYRO,BIAS_GYRO)   = gyro_bias_std * gyro_bias_std * Eigen::Matrix3d::Identity();          // Bias Gyro
#endif

#ifdef BIAS_GRAV
  filt.P.block<3,3>(BIAS_GRAV,BIAS_GRAV) = Eigen::Matrix3d::Identity();
#endif

#ifdef BARO_OFFSET
  filt.P(BARO_OFFSET,BARO_OFFSET) = 0.25;
#endif

#ifdef PREV_POS
  filt.P.block<3,3>(PREV_POS,PREV_POS)   = filt.P.block<3,3>(POSITION,POSITION);
#endif

#ifdef PREV_ERV
  filt.P.block<3,3>(PREV_ERV,PREV_ERV)   = filt.P.block<3,3>(ERV,ERV);
#endif

  // Check if any of these initial values are invalid and fix them
  filt.checkInitialization();

  publishOdom(initOdom.header.stamp);

  // Unsubscribe to the initialization msg since we only want to do this once
  initSub_.shutdown();
  filt.isInit = true;
  filt.initTime = initOdom.header.stamp.toSec();

  // Insert our first buffered filter instance into the list
  filtInst ss(filt);
  ss.pose.time = initOdom.header.stamp.toSec();

  std::multimap<double,filtInst>::iterator it = filtList.insert(std::pair<double,filtInst>(ss.pose.time,ss));

  // Erase all the older data that has come before the initialization
  filtList.erase(filtList.begin(), it);

  ROS_INFO_STREAM("UKF initialized: " << filt.x.transpose());
  ROS_INFO_STREAM("Time: " << initOdom.header.stamp);
  ROS_INFO_STREAM("Data buffer size: " << filtList.size());

  if (filtList.size() > 1) {
    // Process all the newer data
    rerunSensorBuffer(++it);
    ROS_INFO_STREAM("UKF processed to: " << filt.x.transpose());
  }
  return;
}

//template <class msgT>
//void NodeImuVo::getTransforms(msgT msg){
//  if(useVisualOdometry){
//  if (!CA::findTransformation(listener_, imu_frame_id_, camera_frame_id_, msg->header.stamp, ros::Duration(0.5), filt.Tic))
//    return;
//    }
//  if (!CA::findTransformation(listener_, imu_frame_id_, vehicle_frame_id_, msg->header.stamp, ros::Duration(0.5), Tiv))
//    return;
//  if(useGPS){
//  if (!CA::findTransformation(listener_, imu_frame_id_, antenna_frame_id_, msg->header.stamp, ros::Duration(0.5), filt.Tig))
//    return;
//  }
//return;
//}

void NodeImuVo::voCallback(const nav_msgs::Odometry::ConstPtr& msg,
    const ca_common::BoolStamped::ConstPtr& newFrameMsg) {
  // Store the msg in the filtList
  std::multimap<double,filtInst>::iterator it = spliceInBuffer(msg, newFrameMsg->data);

  // Only continue if the filter is initialized
  if (filt.isInit && (msg->header.stamp.toSec() > filt.initTime)) {
    // If the incoming msg was valid,
    // Rewind the filtList to time in current msg
    rerunSensorBuffer(it);
  }
  return;
}

template <class msgT>
void NodeImuVo::templateCallback(msgT msg) {
  // Store the msg in the filtList
  std::multimap<double,filtInst>::iterator it = spliceInBuffer(msg);
  // Only continue if the filter is initialized
  if (filt.isInit && (msg->header.stamp.toSec() > filt.initTime)) {
    // If the incoming msg was valid,
    // Rewind the filtList to time in current msg
    rerunSensorBuffer(it);
  }
  return;
}

template <class msgT>
std::multimap<double, filtInst>::iterator NodeImuVo::spliceInBuffer(msgT msg, bool hasNewFrame) {
  // filtList runs from oldest to youngest. The most recent msgs are at the end of the list.
  std::multimap<double, filtInst>::iterator it;

  filtInst ss(filt);
  ss.pose.time = msg->header.stamp.toSec();
  bool success = storeSensorMsg(ss, msg, hasNewFrame);
  if (success) {
    it = filtList.insert(std::pair<double, filtInst>(ss.pose.time, ss));
    return it;
  } else {
    return filtList.end();
  }
}

/*
 * Store different types of sensor messages
 */
// IMU msg
bool NodeImuVo::storeSensorMsg(filtInst &ss, const sensor_msgs::Imu::ConstPtr& msg, bool hasNewFrame) {
  return sensorImu.storeSensorMsg(ss, msg, hasNewFrame);
}

bool NodeImuVo::storeSensorMsg(filtInst &ss, const spk_weather_board::AirDensityHeight::ConstPtr& msg, bool hasNewFrame) {
  return sensorWeatherBd.storeSensorMsg(ss, msg, hasNewFrame);
}

bool NodeImuVo::storeSensorMsg(filtInst &ss, const ca_common::GroundPlaneHRP::ConstPtr& msg, bool hasNewFrame) {
  return sensorAgl.storeSensorMsg(ss, msg, hasNewFrame);
}

// FC Debug msg with height data
bool NodeImuVo::storeSensorMsg(filtInst &ss, const mikrokopter::FcDebug::ConstPtr& msg, bool hasNewFrame) {
  return sensorFcDebug.storeSensorMsg(ss, msg, hasNewFrame);
}

// GPS or VO odometry msg
bool NodeImuVo::storeSensorMsg(filtInst &ss, const nav_msgs::Odometry::ConstPtr& msg, bool hasNewFrame) {
  size_t found;
  found = msg->child_frame_id.find("gps");

  // VO message
  if (msg->child_frame_id.length() > 0 && found == std::string::npos) {
    return sensorVisualOdometry.storeSensorMsg(ss, msg, hasNewFrame);
  } else { // GPS message

    gps_frame_ = msg->header.frame_id;

    //filt.setNewGpsRef = false;
    //static double ref_gps_var = 10000;
    //if (msg->pose.covariance[0] < sensorGps.maxGpsStdev*sensorGps.maxGpsStdev && (sensorGps.hasFailed || (msg->pose.covariance[0] < ref_gps_var)))
    if (msg->pose.covariance[0] > 0 && msg->pose.covariance[0] < sensorGps.maxGpsStdev*sensorGps.maxGpsStdev && sensorGps.hasFailed) {
      sensorGps.hasFailed = false;
      //ref_gps_var = msg->pose.covariance[0];
      filt.setNewGpsRef = true;
      ss.pose.localFrameValid = false;
    }

    nav_msgs::Odometry transformedOdom;
    if (transformOdometryMsg(reference_frame_, *msg, transformedOdom, listener_))
    {
      if (sensorGps.gpsDiag) {
        sensorGps.gpsDiag->tick(msg->header.stamp);
      }
      ss.gpsMsg = boost::make_shared<nav_msgs::Odometry const>(transformedOdom);
      sensorGps.isAlive = true;
    } else {
      ROS_FATAL_STREAM("Transforming GPS msg to reference frame failed!");
      sensorGps.isAlive = false;
      return false;
    }
  }
  return true;
}


void NodeImuVo::rerunSensorBuffer(std::multimap<double,filtInst>::iterator it)
{
  if (it == filtList.end())
  {
    ROS_INFO_STREAM("rerunSensorBuffer: iterator is already at the end of the sensor data buffer" );
    error_msg="iterator is already at the end of the sensor data buffer";
//    updater.update();    
    refiner->AddRefinerEntry("rerunSensorBuffer1", 2,error_msg);
    return;
  }

  // Reset the filter to the pose right before the new data.
  if (it != filtList.begin())
  {
    --it;
    filt = it->second.pose;
  }
  else
  {
    // If the latest msg fits in the beginning of filt List which is the last time stamp in the buffer.
    ROS_ERROR_STREAM("Data is too old!"<<it->first-947629726.457351<<" , "<<ros::Time::now());
    error_msg="Data is too old!"+boost::to_string(it->first-947629726.457351)+" , "+boost::to_string(ros::Time::now());
//    updater.update();
    refiner->AddRefinerEntry("rerunSensorBuffer2",2, error_msg, ros::Duration(1));
    filtList.erase(it);
    return;
  }


  // Move back to the new data instance and begin to update
  ++it;

  // Iterate through the sensor buffer and update the poses
  for ( ; it != filtList.end(); ++it)
  {

    filt.dt = it->first - filt.time;
    if (filt.dt <= 0)
    {
      //ROS_ERROR_STREAM("IMUVO: Error processing out of order sensor message. dt: " << filt.dt);
      error_msg="Error processing out of order sensor message. dt: "+boost::to_string(filt.dt);
//      updater.update();
      refiner->AddRefinerEntry("rerunSensorBuffer3", 2, error_msg);

      pet.fault();
    }
    else
    {
      //This imu msg should be processed before prediction quaternion wrapper
      if (it->second.imuMsg)
      {
        sensorImu.processSensorMsg(it->second.imuMsg, filt);
      }
      // PREDICTION CYCLE
      sensorImu.extrapolateImuForward(filt);
      sensorWeatherBd.extrapolateBarometricForward(filt);
      filt.predictionQuaternionWrapper();

      // CORRECTION CYCLE
      if (it->second.imuMsg)
      {
        sensorImu.processSensorMsg(it->second.imuMsg, filt);
      }
      // VISUAL ODOMETRY
      else if (it->second.voMsg)
      {
        if(!sensorVisualOdometry.processSensorMsg(it->second.voMsg, it->second.newFrame, filt))
        {
          error_msg="Visual Odometry msg rejected";
//          updater.update();
          refiner->AddRefinerEntry("rerunSensorBuffer4", 2, error_msg,ros::Duration(2));
          ROS_ERROR_STREAM(error_msg);
        }
        if (!useGPS && useCovarianceScaling)
          filt.P = filt.P/1.01; // Need to scale the covariance when not using GPS

        if (useVelocityInflation)
          filt.P.block<3,3>(VELOCITY,VELOCITY) += 0.01*Eigen::Matrix3d::Identity();

        if (useErvInflation)
          filt.P.block<3,3>(ERV,ERV) += 0.0001*Eigen::Matrix3d::Identity();

      }
      // FC DEBUG (not used if we have the weather board)
      else if (it->second.fcMsg)
      {
        sensorFcDebug.processSensorMsg(it->second.fcMsg, filt);
      }
      // GPS
      else if (it->second.gpsMsg)
      {
        if(!sensorGps.processSensorMsg(it->second.gpsMsg, filt))
        {
          error_msg="GPS msg rejected";
//          updater.update();
          refiner->AddRefinerEntry("rerunSensorBuffer5", 2, error_msg,ros::Duration(2));
          ROS_ERROR_STREAM(error_msg);
        }
        if (useErvInflation)
        {
          // Add general uncertainty to attitude
          filt.P.block<3,3>(ERV,ERV) += 0.0001*Eigen::Matrix3d::Identity();

          // If we are using visual odometry, add more uncertainty to yaw. This will allow VO to correct heading error easier
          if (!sensorVisualOdometry.isAlive)
          {
            //    filt.P(ERV+2,ERV+2) += 0.001; // Add roughly 2 deg of uncertainty
            filt.P(ERV+2,ERV+2) += 0.01; // Add roughly 5 deg of uncertainty
          }
          else
            filt.P(ERV+2,ERV+2) += 0.0004; // Add roughly 1 deg of uncertainty
        }
      }
      // WEATHER BOARD
      else if (it->second.heightMsg)
      {
        if(!sensorWeatherBd.processSensorMsg(it->second.heightMsg, filt))
        {
          error_msg="Barometer msg rejected";
//          updater.update();   
          refiner->AddRefinerEntry("rerunSensorBuffer6",2, error_msg,ros::Duration(2));
          ROS_ERROR_STREAM(error_msg);
        }
      }
      // GROUND PLANE HEIGHT
      else if (it->second.gpHeightMsg)
      {
        if(!sensorAgl.processSensorMsg(it->second.gpHeightMsg, filt))
        {
          error_msg="AGL msg rejected";
//          updater.update();
          refiner->AddRefinerEntry("rerunSensorBuffer7",2, error_msg,ros::Duration(2));
          ROS_ERROR_STREAM(error_msg);
        }
      }
      else
      {
        // TODO better handling of this error
        error_msg="IMUVO: No valid sensor data stored in buffer instance!";
//        updater.update();
        refiner->AddRefinerEntry("rerunSensorBuffer8", 2, error_msg,ros::Duration(2));
        ROS_ERROR_STREAM(error_msg);
      }
    }
      publishOdom(ros::Time(it->first));
      filt.time = it->first;
      // Write the updated filterInstance to this location in the list
      it->second.pose.dt=0;
      it->second.pose = filt;


  }

  // Delete elements that are too old
  std::multimap<double, filtInst>::iterator itlow = filtList.lower_bound((ros::Time::now() - ros::Duration(storageBuffer_s)).toSec());

  filtList.erase ( filtList.begin(), itlow );

  return;
}

/*
 * publishOdom - Publishes an odometry message from the UKF output
 */
void NodeImuVo::publishOdom(ros::Time msgTime)
{

  if ( (msgTime.toSec() > mostRecentTime) || publishPrevStates)
  {
    mostRecentTime = msgTime.toSec();

    /* Publish the UKF state in its raw form for debugging */
    ukf::State stateMsg;
    stateMsg.header.stamp = msgTime;
    for (int i = 0; i < filt.x.size(); i++)
    {
      stateMsg.x.push_back(filt.x(i));
    }
    for (int i = 0; i < filt.P.size(); i++)
    {
      stateMsg.P.push_back(filt.P(i));
    }
    statePublisher.publish(stateMsg);


    /* Publish the ROS odometry msg */
    nav_msgs::Odometry refToVehicle;

    refToVehicle.header.stamp = msgTime;
    refToVehicle.header.frame_id = reference_frame_;
    refToVehicle.child_frame_id = vehicle_frame_id_;


    // Transform the odometry message to the vehicle coordinate frame
    // Twi: World to IMU transform
    // Tiv: IMU to vehicle transform
    // Twv: World to vehicle transform

    Eigen::Isometry3d Twi(filt.q_w_i);
    Twi.translation() = filt.getPosition();

    Eigen::Isometry3d Twv = Twi*Tiv;//Tiv*Twi;
  //ROS_INFO_STREAM("position change: "<<Twi.translation().transpose()<<"--,--"<<Twv.translation().transpose());
    // TODO: How do I perform a coordinate system transform on covariance matrices?
    Eigen::Vector3d t = Twv.translation();
    refToVehicle.pose.pose.position = ca::point_cast<geometry_msgs::Point>(t);
    refToVehicle.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(Eigen::Quaterniond(Twv.linear()));
    imu_filter::message_helper::eigenToArray(filt.P.block<6,6>(POSITION,POSITION), refToVehicle.pose.covariance);
    Eigen::Vector3d vehicleBodyVelocity;
#ifdef LEVERARM
    // Calculate the lever-arm effect between the IMU and vehicle and add in the resulting velocity component from angular rotation
    // Transform to the vehicle body frame
    vehicleBodyVelocity = Tiv.linear().transpose()*(filt.getVelocity() + filt.gyroMeasurement.cross(Tiv.translation()));

    refToVehicle.twist.twist.linear = ca::point_cast<geometry_msgs::Vector3>(vehicleBodyVelocity);
    refToVehicle.twist.twist.angular = ca::point_cast<geometry_msgs::Vector3>(Tiv.linear().transpose()*filt.gyroMeasurement);
#else 
    vehicleBodyVelocity = filt.getVelocity();
    refToVehicle.twist.twist.linear = ca::point_cast<geometry_msgs::Vector3>(vehicleBodyVelocity);
    refToVehicle.twist.twist.angular = CA::msgc(Tiv.linear().transpose()*filt.gyroMeasurement);
#endif
    Eigen::Matrix<double,6, 6> twistCovar;
    twistCovar.block<3,3>(0,0) = filt.P.block<3,3>(VELOCITY,VELOCITY);
    twistCovar.block<3,3>(3,3) = filt.Q.block<3,3>(3,3);
    imu_filter::message_helper::eigenToArray(twistCovar, refToVehicle.twist.covariance);

    if(vehicleBodyVelocity.norm()>max_velocity){
      error_msg="The output velocity magnitude "+boost::to_string(vehicleBodyVelocity.norm())+"m/s is greater than "+boost::to_string(max_velocity);
      refiner->AddRefinerEntry("velocity limit",2, error_msg,ros::Duration(5));
      ROS_ERROR_STREAM(error_msg);
    }
    worldToVehicleOdomPub_.publish(refToVehicle);


    /* Publish the ROS odometry msg
    nav_msgs::Odometry refToVehicle;

    refToVehicle.header.stamp = msgTime;
    refToVehicle.header.frame_id = reference_frame_;
    refToVehicle.child_frame_id = imu_frame_id_;


    // Transform the odometry message to the vehicle coordinate frame
    refToVehicle.pose.pose.position = CA::msgcp(filt.getPosition());
    refToVehicle.pose.pose.orientation = CA::msgc(filt.q_w_i);

    imu_filter::message_helper::eigenToArray(filt.P.block<6,6>(POSITION,POSITION), refToVehicle.pose.covariance);

    refToVehicle.twist.twist.linear = CA::msgc(filt.getVelocity());
    refToVehicle.twist.twist.angular = CA::msgc(filt.gyroMeasurement);

    Eigen::Matrix<double,6, 6> twistCovar = Eigen::Matrix<double,6, 6>::Zero();
    twistCovar.block<3,3>(0,0) = filt.P.block<3,3>(VELOCITY,VELOCITY);
    twistCovar.block<3,3>(3,3) = filt.Q.block<3,3>(3,3);
    imu_filter::message_helper::eigenToArray(twistCovar, refToVehicle.twist.covariance);

    worldToVehicleOdomPub_.publish(refToVehicle);
*/

#ifdef DRIFT
    // Publish drift information
    Eigen::Isometry2d localDrift = Eigen::Isometry2d::Identity();
    double theta = filt.getDrift()(2);
    localDrift.rotate(theta);
    localDrift.translate(filt.getDrift().segment<2>(0));

    // T_ned_ref transform from ned to the reference frame (typically "world")
    // Would also transform points in the reference frame back to the ned frame
    Eigen::Isometry2d T_ned_ref = filt.prevGlobalDrift * filt.q_N * localDrift * filt.q_N.inverse();
    Eigen::Isometry2d T_ref_ned = T_ned_ref.inverse();

    //Eigen::Isometry2d T_vehicle_ned = Tiv.inverse() * T_ned_imu.inverse();

    nav_msgs::Odometry driftOdom;

    driftOdom.header.stamp = msgTime;
    driftOdom.header.frame_id = reference_frame_;    // Typically "/world"
    driftOdom.child_frame_id = gps_frame_;      // Typically "/ned_orig"

    driftOdom.pose.pose.position.x = T_ref_ned.translation()(0);
    driftOdom.pose.pose.position.y = T_ref_ned.translation()(1);
    driftOdom.pose.pose.position.z = 0;

    Eigen::Rotation2Dd rot(0);
    rot.fromRotationMatrix(T_ref_ned.linear());

    driftOdom.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(CA::eulerToQuat(Eigen::Vector3d(0, 0, rot.angle())));
    driftPub.publish(driftOdom);



    Eigen::Vector2d expectedGpsXY = filt.measurementModel_GPS_2DOF(filt.x);
    nav_msgs::Odometry correctedXYZ;

    correctedXYZ.header.stamp = msgTime;
    correctedXYZ.header.frame_id = gps_frame_;
    correctedXYZ.child_frame_id = vehicle_frame_id_;

    correctedXYZ.pose.pose.position.x = expectedGpsXY(0);
    correctedXYZ.pose.pose.position.y = expectedGpsXY(1);
    correctedXYZ.pose.pose.position.z = refToVehicle.pose.pose.position.z;

    correctPosePub.publish(correctedXYZ);
#endif

  }

  /* Pet the watchdog */
  pet.alive();

  return;
}

bool NodeImuVo::transformOdometryMsg(const std::string& target_frame, const nav_msgs::Odometry& odom_in, nav_msgs::Odometry& odom_out, tf::TransformListener* listener)
{
  bool validTF = listener->waitForTransform(target_frame, odom_in.header.frame_id, odom_in.header.stamp, ros::Duration(0.2) );
  if (validTF)
  {
    tf::StampedTransform dtransform;
    listener->lookupTransform(target_frame, odom_in.header.frame_id, odom_in.header.stamp, dtransform);

    // Convert the TF transform to an Eigen transform
    Eigen::Affine3d af;
    tf::transformTFToEigen(dtransform, af);

    Eigen::Isometry3d itf;

    itf.matrix() = af.matrix();

    odom_out = odom_in;
    odom_out.header.frame_id = target_frame;

    odom_out.pose.pose = CA::msgc(itf * Eigen::Isometry3d(CA::msgc(odom_in.pose.pose)));


    //ROS_INFO_STREAM(ros::this_node::getName() << ": Found " << target_frame << " to " << odom_in.header.frame_id << " transform:\n" << af.matrix());
  }
  else
  {
    ROS_ERROR_STREAM(ros::this_node::getName() << ": Cannot find transform from " << target_frame << " to " << odom_in.header.frame_id << ".");
    error_msg=ros::this_node::getName()+": Cannot find transform from "+target_frame + " to " + odom_in.header.frame_id + ".";
//    updater.update();
    refiner->AddRefinerEntry("Transform", 2,error_msg);
  }

  return validTF;
}


void NodeImuVo::watchdogCallback(const ros::TimerEvent& event)
{
  bool newuseVisualOdometry;
  np_.getParamCached("useVisualOdometry", newuseVisualOdometry);
  updater.update();
  if (newuseVisualOdometry != useVisualOdometry)
  {

    useVisualOdometry = newuseVisualOdometry;

    sensorVisualOdometry.isInitialized = false;

    if (useVisualOdometry)
    {
      ROS_INFO("Enabling VO");

      if (sensorVisualOdometry.useKeyFrames)
      {
        vo_sub.subscribe(n_,"input/vo",100);
        newFrameSub.subscribe(n_,"/kfmatcher/hasNewFrame",100);

        sync->connectInput(vo_sub, newFrameSub);
        sync->registerCallback(boost::bind(&NodeImuVo::voCallback, this, _1, _2));

        std::cout << "Registered VO Keyframe TimeSynchronizer" << std::endl;
      }
      else
      {
        sensorVisualOdometry.subscriber = n_.subscribe<nav_msgs::Odometry>("input/vo", 100, &NodeImuVo::templateCallback, this);  // I don't want to lose any VO messages
        std::cout << "Registered VO subscriber" << std::endl;
      }
    }
    else
    {
      ROS_INFO("Disabling VO");
      sensorVisualOdometry.subscriber.shutdown();
    }


  }

  bool newUseGPS;
  np_.getParamCached("useGPS", newUseGPS);

  if (newUseGPS != useGPS)
  {
    useGPS = newUseGPS;

    ros::TransportHints ros_transport_hints = ros::TransportHints().udp().tcpNoDelay();

    if (useGPS)
    {
      ROS_INFO("Enabling GPS");
      sensorGps.subscriber = n_.subscribe<nav_msgs::Odometry>("input/gps", 100, &NodeImuVo::templateCallback, this, ros_transport_hints);
    }
    else
    {
      ROS_INFO("Disabling GPS");
      sensorGps.subscriber.shutdown();
    }

  }
  bool newUseGroundPlane;
  np_.getParamCached("useGroundPlane", newUseGroundPlane);

  if (newUseGroundPlane != useGroundPlane)
  {
    useGroundPlane = newUseGroundPlane;

    ros::TransportHints ros_transport_hints = ros::TransportHints().udp().tcpNoDelay();

    if (useGroundPlane)
    {
      ROS_INFO("Enabling GroundPlane");
      sensorAgl.subscriber = n_.subscribe<ca_common::GroundPlaneHRP>("input/hrp", 100, &NodeImuVo::templateCallback, this, ros_transport_hints);
    }
    else
    {
      ROS_INFO("Disabling GPS");
      sensorAgl.subscriber.shutdown();
    }

  }
  if (useGPS)
  {
    if (sensorGps.isAlive)
    {
      sensorGps.isAlive = false;
    }
    else
    {
      ROS_ERROR_STREAM_THROTTLE(1, "navframe: No GPS msgs received!" );
      sensorGps.hasFailed = true;
      pet.fault();
    }
  }


  if (useBaro)
  {
    if (sensorWeatherBd.isAlive)
    {
      sensorWeatherBd.isAlive = false;
    }
    else
    {
      ROS_ERROR_STREAM_THROTTLE(1, "navframe: No barometric msgs received!" );
      pet.fault();
    }
  }

  if (useGroundPlane)
  {
    if (sensorAgl.isAlive)
    {
      sensorAgl.isAlive = false;
    }
    else
    {
      ROS_ERROR_STREAM_THROTTLE(1, "navframe: No ground plane height msgs received!" );
      pet.fault();
    }
  }

  if (useVisualOdometry)
  {
    if (sensorVisualOdometry.isAlive)
    {
      sensorVisualOdometry.isAlive = false;
    }
    else
    {
      ROS_ERROR_STREAM_THROTTLE(1, "navframe: No visual odometry msgs received!!!!!!!!!!!!!!" );
      pet.fault();
    }
  }

  if (sensorImu.isAlive)
  {
    sensorImu.isAlive = false;
  }
  else
  {
    ROS_ERROR_STREAM_THROTTLE(1, "navframe: No IMU msgs received!" );
    pet.fault();
  }

  if (!filt.isInit)
  {
    ROS_WARN_THROTTLE(1, "UKF IMUVO: Kalman filter NOT initialized yet. Waiting...");
    refiner->AddRefinerEntry("UKF IMUVO", 2,"Kalman filter NOT initialized yet. Waiting...");
    pet.fault();
  }

}



