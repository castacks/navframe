/*
* Copyright (c) 2016 Carnegie Mellon University, Author <achamber>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * navframe.cpp
 *
 *  Created on: Jan 23, 2012
 *      Author: achamber
 */

// 6 DOF movement using quaternions and IMU mechanization
// Delayed state for previous position and quaternion

#include <navframe/navframe.h>
Eigen::VectorXd ImuVo::predictionModel(const Eigen::VectorXd &x)
{
  assert (x.rows() == LENGTH_STATE_VECTOR + LENGTH_NOISE_VECTOR);

  // Handle the quaternions

  //  // Convert the error rotation vector to a quaternion error
  Eigen::Quaterniond q_error = rotVecToQuat(x.segment<3>(ERV));

  // Find the quaternion from world to IMU for this particular sigma point
  Eigen::Quaterniond sigma_q_w_i = q_error*q_w_i;

  // Find the true angular velocity in the IMU frame by removing the noise and bias from the measurement.
#ifdef BIAS_GYRO
  Eigen::Vector3d real_w = gyroMeasurement - x.segment<3>(BIAS_GYRO) - x.segment<3>(NOISE_GYRO + LENGTH_STATE_VECTOR); // rad/s
//  ROS_INFO_STREAM("gyro   "<<i<<" : "<<x.segment<3>(BIAS_GYRO).transpose()<<"  ,  "<<x.segment<3>(NOISE_GYRO + LENGTH_STATE_VECTOR).transpose());
//  ROS_INFO_STREAM("gyro w "<<i<<" : "<<real_w.transpose()*dt);
//  ROS_INFO_STREAM("gyro q "<<i<<" : "<<temp.x()<<" , "<<temp.y()<<" , "<<temp.z()<<" , "<<temp.w());
#else
  Eigen::Vector3d real_w = gyroMeasurement - x.segment<3>(NOISE_GYRO + LENGTH_STATE_VECTOR); // rad/s
#endif

//      Eigen::Quaterniond angular_rotation(rotVecToQuat(real_w*dt));
  // Find the time derivative for the quaternion based on the true angular rate
  Eigen::Matrix3d skew_w = CA::math_tools::skewSymmetric(real_w);

  Eigen::Matrix4d omega_w;
  omega_w <<   -skew_w,         real_w,
      -real_w.transpose(),   0;


  // quaternion.coeffs() produces x,y,z,w
  Eigen::Vector4d dq_dt = 0.5*omega_w*sigma_q_w_i.coeffs();
  // Perform the rate update for the quaternion.
  // the rotation from world to IMU for this sigma point
  // Constructor for quaternion is: Quaternion (Scalar w, Scalar x, Scalar y, Scalar z)
  // It can be constructed as Quaternion (const MatrixBase< Derived > &other) but it expects the ordering to be xyzw
  
  Eigen::Quaterniond sigma_qk1(sigma_q_w_i.coeffs() + dt*dq_dt);
//      Eigen::Quaterniond sigma_qk1=angular_rotation*sigma_q_w_i;

  if (!sigma_qk1_valid)
  {
    sigma_qk1_0 = Eigen::Quaterniond( q_w_i.coeffs() + dt*0.5*omega_w*q_w_i.coeffs() );
    sigma_qk1_0.normalize();
//              sigma_qk1_0 = angular_rotation*q_w_i;
    sigma_qk1_valid = true;
  }

  sigma_qk1.normalize();
  Eigen::Quaterniond q_error_k1 = sigma_qk1*sigma_qk1_0.inverse();

  
  // Convert the error quaternion to an error rotation vector
  if (isnan(q_error_k1.coeffs().norm()))
    std::cout << "NaN in prediction - sigma_qk1: " << sigma_qk1.coeffs().transpose() << " sigma_qk1_0: " << sigma_qk1_0.coeffs().transpose() << std::endl;
  Eigen::Vector3d rotvec_error_k1 = quat2rotvec(q_error_k1);

  // Find the true acceleration in the IMU frame by removing the noise, bias, and gravity vector from the measurement.
#ifdef BIAS_ACCEL
  Eigen::Vector3d true_body_accel = accelMeasurement - x.segment<3>(BIAS_ACCEL) - x.segment<3>(NOISE_ACCEL + LENGTH_STATE_VECTOR) + sigma_qk1.inverse()*gravity;
  //  Eigen::Vector3d real_accel = sigma_qk1*(accelMeasurement - x.segment<3>(BIAS_ACCEL) - x.segment<3>(NOISE_ACCEL + LENGTH_STATE_VECTOR)) + gravity;
#else
  Eigen::Vector3d true_body_accel = accelMeasurement - x.segment<3>(NOISE_ACCEL + LENGTH_STATE_VECTOR) + sigma_qk1.inverse()*gravity;
  //  Eigen::Vector3d real_accel = sigma_qk1*(accelMeasurement - x.segment<3>(NOISE_ACCEL + LENGTH_STATE_VECTOR)) + gravity;
#endif


  // Predict state vector at the next time step
  Eigen::Matrix<double, LENGTH_STATE_VECTOR, 1> xk1 = x.segment<LENGTH_STATE_VECTOR>(0);

  xk1.segment<3>(POSITION)   = x.segment<3>(POSITION) + sigma_qk1*x.segment<3>(VELOCITY)*dt;// + 0.5*(sigma_qk1*true_body_accel)*dt*dt;
  xk1.segment<3>(ERV)      = rotvec_error_k1;
  xk1.segment<3>(VELOCITY)   = x.segment<3>(VELOCITY) + true_body_accel*dt;
#ifdef BIAS_ACCEL
  xk1.segment<3>(BIAS_ACCEL)   = x.segment<3>(BIAS_ACCEL) + x.segment<3>(NOISE_BA + LENGTH_STATE_VECTOR)*dt;
  //  xk1.segment<3>(BIAS_ACCEL)   = x.segment<3>(BIAS_ACCEL) + (-x.segment<3>(BIAS_ACCEL)/accel_bias_tc + x.segment<3>(NOISE_BA + LENGTH_STATE_VECTOR))*dt;
#endif

#ifdef BIAS_GYRO
  xk1.segment<3>(BIAS_GYRO)   = x.segment<3>(BIAS_GYRO) + x.segment<3>(NOISE_BG + LENGTH_STATE_VECTOR)*dt;
  //  xk1.segment<3>(BIAS_GYRO)   = x.segment<3>(BIAS_GYRO) +  (-x.segment<3>(BIAS_GYRO) /gyro_bias_tc  + x.segment<3>(NOISE_BG + LENGTH_STATE_VECTOR))*dt;
#endif

#ifdef BIAS_GRAV
  xk1.segment<3>(BIAS_GRAV)   = sigma_qk1.inverse()*gravity;
#endif

#ifdef BARO_OFFSET
  xk1(BARO_OFFSET)   = x(BARO_OFFSET) + x(NOISE_BARO_OFFSET + LENGTH_STATE_VECTOR)*dt;
#endif

  return xk1;
}

Eigen::VectorXd ImuVo::measurementModel(const Eigen::VectorXd &x)
{

  Eigen::VectorXd z_expected;

  switch (measurementType)
  {
  case VO:
    z_expected = measurementModel_VO(x);
    break;
  case VO_POS_VEL:
    z_expected = measurementModel_VO_POS_VEL(x);
    break;
  case VO_VEL:
    z_expected = measurementModel_VO_VEL(x);
    break;
  case VO_ABS:
    z_expected = measurementModel_VO_ABS(x);
    break;
  case HEIGHT:
    z_expected = measurementModel_HEIGHT(x);
    break;
  case GPS_2DOF:
    z_expected = measurementModel_GPS_2DOF(x);
    break;
  case GPS_3DOF:
    z_expected = measurementModel_GPS_3DOF(x);
    break;
  case GPS_VEL_4DOF:
    z_expected = measurementModel_GPS_VEL_4DOF(x);
    break;
  case GPS_VEL_5DOF:
    z_expected = measurementModel_GPS_VEL_5DOF(x);
    break;
  case GPS_VEL_6DOF:
    z_expected = measurementModel_GPS_VEL_6DOF(x);
    break;
  case GRAV:
    z_expected = measurementModel_GRAV(x);
    break;
  default:
    ROS_FATAL_STREAM("Measurement type " << measurementType << " does not exist!");
    break;
  }

  return z_expected;

}

Eigen::VectorXd ImuVo::measurementModel_VO(const Eigen::VectorXd &x)
{
  // Convert the error rotation vector to a quaternion error
  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));
  Eigen::Quaterniond prev_qwi_error = rotVecToQuat(x.segment<3>(PREV_ERV));

  // Find the resulting quaternion
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;
  Eigen::Quaterniond prev_sigma_qwi = prev_qwi_error*prev_q_w_i;


  // Create a 4x4 isometry transform
  // from the world to the current IMU position
  Eigen::Isometry3d Twi(sigma_qwi);
  Twi.translation() = x.segment<3>(POSITION);

  // from the world to the previous IMU position
  Eigen::Isometry3d prevTwi(prev_sigma_qwi);
  prevTwi.translation() = x.segment<3>(PREV_POS);

  // Find the transform from world to the camera positions using the known IMU to camera calibration
  Eigen::Isometry3d Twc     = Twi*Tic;
  Eigen::Isometry3d prevTwc   = prevTwi*Tic;

  // Find the expected transform from the current camera pose to the previous camera pose
  Eigen::Isometry3d T_c_prevc = Twc.inverse() * prevTwc;

  // Create the expected measurement vector
  //  Eigen::VectorXd z_expected(6);
  Eigen::VectorXd z_expected(6);

  z_expected.segment<3>(0) = T_c_prevc.translation();
  z_expected.segment<3>(3) = quat2rotvec(Eigen::Quaterniond(T_c_prevc.linear()));

  if (isnan(T_c_prevc.linear().norm()))
    std::cout << "NaN in VO correction - T_c_prevc.linear(): " << T_c_prevc.linear() << std::endl;

  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_VO_POS_VEL(const Eigen::VectorXd &x)
{
  // Convert the error rotation vector to a quaternion error
  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));
  Eigen::Quaterniond prev_qwi_error = rotVecToQuat(x.segment<3>(PREV_ERV));

  // Find the resulting quaternion
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;
  Eigen::Quaterniond prev_sigma_qwi = prev_qwi_error*prev_q_w_i;


  // Create a 4x4 isometry transform
  // from the world to the current IMU position
  Eigen::Isometry3d Twi(sigma_qwi);
  Twi.translation() = x.segment<3>(POSITION);

  // from the world to the previous IMU position
  Eigen::Isometry3d prevTwi(prev_sigma_qwi);
  prevTwi.translation() = x.segment<3>(PREV_POS);

  // Find the transform from world to the camera positions using the known IMU to camera calibration
  Eigen::Isometry3d Twc     = Twi*Tic;
  Eigen::Isometry3d prevTwc   = prevTwi*Tic;

  // Find the expected transform from the current camera pose to the previous camera pose
  Eigen::Isometry3d T_c_prevc = Twc.inverse() * prevTwc;

  // Create the expected measurement vector
  //  Eigen::VectorXd z_expected(6);
  Eigen::VectorXd z_expected(9);

  z_expected.segment<3>(0) = T_c_prevc.translation();
  if (isnan(T_c_prevc.linear().norm()))
    std::cout << "NaN in VO correction - T_c_prevc.linear(): " << T_c_prevc.linear() << std::endl;

  z_expected.segment<3>(3) = quat2rotvec(Eigen::Quaterniond(T_c_prevc.linear()));

  // Find the true angular velocity in the IMU frame by removing the noise and bias from the measurement.
#ifdef BIAS_GYRO
  Eigen::Vector3d real_w = gyroMeasurement - x.segment<3>(BIAS_GYRO); // rad/s
#else
  Eigen::Vector3d real_w = gyroMeasurement; // rad/s
#endif

  z_expected.segment<3>(6) = Tic.linear().transpose()*(-1*x.segment<3>(VELOCITY) + real_w.cross(Tic.translation()));
  //  z_expected.segment<3>(6) = Tic.linear().transpose()*(-1*x.segment<3>(VELOCITY)); // Negative since VO reports current to previous frame velocity

  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_VO_VEL(const Eigen::VectorXd &x)
{
  Eigen::Vector3d z_expected = Tic.linear().transpose()*(getVelocity());
  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_VO_ABS(const Eigen::VectorXd &x)
{
  // Convert the error rotation vector to a quaternion error
  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));

  // Find the resulting quaternion
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;

  // Create a 4x4 isometry transform
  // from the world to the current IMU position
  Eigen::Isometry3d Twi(sigma_qwi);
  Twi.translation() = x.segment<3>(POSITION);

  // Find the transform from world to the camera positions using the known IMU to camera calibration
  Eigen::Isometry3d Twc     = Twi*Tic;
  Eigen::Isometry3d prevTwc   = abs_prev_Twi*Tic;

  // Find the expected transform from the current camera pose to the previous camera pose
  Eigen::Isometry3d T_c_prevc = Twc.inverse() * prevTwc;

  // Create the expected measurement vector
  Eigen::VectorXd z_expected(6);
  z_expected.segment<3>(0) = T_c_prevc.translation();
  z_expected.segment<3>(3) = quat2rotvec(Eigen::Quaterniond(T_c_prevc.linear()));
  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_HEIGHT(const Eigen::VectorXd &x)
{
  // Create the expected measurement vector
  Eigen::VectorXd z_expected(1);
  z_expected(0) = -x(POSITION+2) + x(BARO_OFFSET);

  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_GPS_2DOF(const Eigen::VectorXd &x)
{
  Eigen::VectorXd z_expected(2);

  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;

#ifdef DRIFT
  Eigen::Isometry2d localDrift = Eigen::Isometry2d::Identity();
  double theta = x(DRIFT+2);
  localDrift.rotate(theta);
  localDrift.translate(x.segment<2>(DRIFT));

  //  Eigen::Isometry2d q_N;
  //  q_N.setIdentity();

  Eigen::Isometry2d globalDrift = prevGlobalDrift * q_N * localDrift * q_N.inverse();
#else
  Eigen::Isometry2d globalDrift = Eigen::Isometry2d::Identity();
#endif

  //  std::cout << "theta " << theta << std::endl;
  //  std::cout << "localDrift" << std::endl;
  //  std::cout << localDrift.matrix() << std::endl;
  //  std::cout << "globalDrift" << std::endl;
  //  std::cout << globalDrift.matrix() << std::endl;

  z_expected = globalDrift*(x.segment<2>(POSITION) + (sigma_qwi*Tig.translation()).segment<2>(0));

  //  z_expected = prevGlobalDrift*x.segment<2>(POSITION);

  //  std::cout << x.segment<2>(POSITION).transpose() << " " << theta << " " << std::endl;
  //  std::cout << "z_expected: " << z_expected.transpose() << std::endl;

  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_GPS_3DOF(const Eigen::VectorXd &x)
{
  Eigen::VectorXd z_expected(3);

  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;

  z_expected = x.segment<3>(POSITION)  + sigma_qwi*Tig.translation();

  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_GPS_VEL_4DOF(const Eigen::VectorXd &x)
{

  Eigen::VectorXd z_expected(4);

  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;

#ifdef DRIFT
  // Estimate the global drift
  Eigen::Isometry2d localDrift = Eigen::Isometry2d::Identity();
  double theta = x(DRIFT+2);
  localDrift.rotate(theta);
  localDrift.translate(x.segment<2>(DRIFT));

  Eigen::Isometry2d globalDrift = prevGlobalDrift * q_N * localDrift * q_N.inverse();
#else
  Eigen::Isometry2d globalDrift = Eigen::Isometry2d::Identity();
#endif

  z_expected.segment<2>(0) = globalDrift*(x.segment<2>(POSITION) + (sigma_qwi*Tig.translation()).segment<2>(0));

  Eigen::Vector3d antennaVelocity = x.segment<3>(VELOCITY) + gyroMeasurement.cross(Tig.translation());

  Eigen::Vector3d localNEDVelocity = sigma_qwi*antennaVelocity;
  z_expected.segment<2>(2) = globalDrift.rotation()*localNEDVelocity.segment<2>(0);

  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_GPS_VEL_5DOF(const Eigen::VectorXd &x)
{
  Eigen::VectorXd z_expected(5);

  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;

#ifdef DRIFT
  // Estimate the global drift
  Eigen::Isometry2d localDrift = Eigen::Isometry2d::Identity();
  double theta = x(DRIFT+2);
  localDrift.rotate(theta);
  localDrift.translate(x.segment<2>(DRIFT));

  Eigen::Isometry2d globalDrift = prevGlobalDrift * q_N * localDrift * q_N.inverse();
#else
  Eigen::Isometry2d globalDrift = Eigen::Isometry2d::Identity();
#endif

  z_expected.segment<2>(0) = globalDrift*(x.segment<2>(POSITION) + (sigma_qwi*Tig.translation()).segment<2>(0));
  z_expected(2) = x(POSITION+2) + (sigma_qwi*Tig.translation())(2);

  Eigen::Vector3d antennaVelocity = x.segment<3>(VELOCITY) + gyroMeasurement.cross(Tig.translation());
  Eigen::Vector3d localNEDVelocity = sigma_qwi*antennaVelocity;
  z_expected.segment<2>(3) = globalDrift.rotation()*localNEDVelocity.segment<2>(0);

  return z_expected;
}

Eigen::VectorXd ImuVo::measurementModel_GPS_VEL_6DOF(const Eigen::VectorXd &x)
{
  Eigen::VectorXd z_expected(6);

  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;

#ifdef DRIFT
  // Estimate the global drift
  Eigen::Isometry2d localDrift = Eigen::Isometry2d::Identity();
  double theta = x(DRIFT+2);
  localDrift.rotate(theta);
  localDrift.translate(x.segment<2>(DRIFT));

  Eigen::Isometry2d globalDrift = prevGlobalDrift * q_N * localDrift * q_N.inverse();
#else
  Eigen::Isometry2d globalDrift = Eigen::Isometry2d::Identity();
#endif

  z_expected.segment<2>(0) = globalDrift*(x.segment<2>(POSITION) + (sigma_qwi*Tig.translation()).segment<2>(0));
  z_expected(2) = x(POSITION+2) + (sigma_qwi*Tig.translation())(2);

  Eigen::Vector3d antennaVelocity = x.segment<3>(VELOCITY) + gyroMeasurement.cross(Tig.translation());
  Eigen::Vector3d localNEDVelocity = sigma_qwi*antennaVelocity;
  z_expected.segment<2>(3) = globalDrift.rotation()*localNEDVelocity.segment<2>(0);

  z_expected(5) = localNEDVelocity(2);

  return z_expected;
}



Eigen::VectorXd ImuVo::measurementModel_GRAV(const Eigen::VectorXd &x)
{

  // Convert the error rotation vector to a quaternion error
  Eigen::Quaterniond qwi_error = rotVecToQuat(x.segment<3>(ERV));

  // Find the resulting quaternion
  Eigen::Quaterniond sigma_qwi = qwi_error*q_w_i;

  Eigen::Vector3d z_expected = sigma_qwi.inverse()*(-1*gravity.normalized());

  //  std::cout << z_expected.transpose() << std::endl;
  return z_expected;
}

Eigen::Quaterniond ImuVo::rotVecToQuat(const Eigen::Vector3d &v)
{
  Eigen::Quaterniond q;

  double angle = v.norm();

  if (angle < MIN_ANGLE)
  {
    q.setIdentity();
    return q;
  }

  Eigen::Vector3d axis = v/angle;


  q.vec() = axis * sin(angle/2);
  q.w() = cos(angle/2);
  q.normalize();
  return q;
}

Eigen::Vector3d ImuVo::quat2rotvec(Eigen::Quaterniond q)
{
  q.normalize();
  double angle = 2 * acos(q.w());
  Eigen::Vector3d axis;

  if (angle < MIN_ANGLE)
  {
    axis = Eigen::Vector3d::Zero();
  }
  else
  {
    axis = q.vec() / sin(angle/2);
    axis.normalize();
  }

  if (isnan(axis(0)))
  {
    std::cout << "quaternion: " << q.coeffs().transpose() << " = axis " << axis.transpose() << " angle " << angle << std::endl;
    axis = Eigen::Vector3d::Zero();
    angle = 0;
    assert(0);
  }

  return (axis*angle);
}


void ImuVo::resetDelayedState(void)
{
  // Set the previous state portion
  this->setPrevPos(this->getPosition());
  prev_q_w_i = q_w_i;

  // Set the covariance
  //  Eigen::MatrixXd tmpP = (sT*P.block<LENGTH_ORIG_STATE,LENGTH_ORIG_STATE>(0,0)*sT.transpose()).eval();

  copyCovar(POSITION+0, PREV_POS+0, P);
  copyCovar(POSITION+1, PREV_POS+1, P);
  copyCovar(POSITION+2, PREV_POS+2, P);

  copyCovar(ERV+0, PREV_ERV+0, P);
  copyCovar(ERV+1, PREV_ERV+1, P);
  copyCovar(ERV+2, PREV_ERV+2, P);

  //  if (!tmpP.isApprox(P, 1e-3))
  //  {
  //    std::cout << tmpP << std::endl << std::endl;
  //    std::cout << tmpP << std::endl;
  //
  //  }


  //  P = (sT*P.block<LENGTH_ORIG_STATE,LENGTH_ORIG_STATE>(0,0)*sT.transpose()).eval();


  lastVO = time;

  return;
}

// transferCovar adds the covariance from src to dst
Eigen::MatrixXd ImuVo::transferCovar(int src, int dst, Eigen::MatrixXd &mat)
{

  // For any two random variables:
  // Var(X+Y)=Var(X)+Var(Y)+2Cov(X,Y)

  // Also
  // COV (X+Y, Z) = COV(X,Z) + COV(Y,Z)

  Eigen::MatrixXd temp = Eigen::MatrixXd::Zero(mat.rows(), mat.cols());
  temp.row(dst) += mat.row(src);
  temp.col(dst) += mat.col(src);
  temp(dst,dst) += mat(src,src);

  temp += mat;

  temp.row(src).setZero();
  temp.col(src).setZero();

  temp(src,src) = 1e-20;

  return temp;


}

#ifdef DRIFT
void ImuVo::cloneLocalFrameCovar(void)
{

  P = transferCovar(POSITION+0, DRIFT+0, P);
  P = transferCovar(POSITION+1, DRIFT+1, P);
  P = transferCovar(ERV+2, DRIFT+2, P);

}

void ImuVo::dropLocalFrame(void)
{

  std::cout << "DROPPING LOCAL FRAME" << std::endl;
  // Update global drift
  Eigen::Vector3d localDriftVector = getDrift();
  //  std::cout << localDriftVector.transpose() << std::endl;

  double theta = localDriftVector(2);

  Eigen::Isometry2d localDriftTransform = Eigen::Isometry2d::Identity();
  localDriftTransform.rotate(theta);
  localDriftTransform.translate(localDriftVector.segment<2>(0));

  prevGlobalDrift = prevGlobalDrift * q_N * localDriftTransform * q_N.inverse();

  // Update q_N
  Eigen::Vector3d euler_w_i = CA::quatToEuler(q_w_i);
  q_N.rotate(euler_w_i(2)); // Rotate by the yaw term

  q_N.translate(getPosition().segment<2>(0));

  // Update covariance matrix
  cloneLocalFrameCovar();

  localFrameValid = true;
}
#endif

void ImuVo::savePrevT(void)
{
  // Create a 4x4 isometry transform
  // from the world to the current IMU position
  abs_prev_Twi = Eigen::Isometry3d(q_w_i);
  abs_prev_Twi.translation() = x.segment<3>(POSITION);
}

void ImuVo::predictionQuaternionWrapper(void)
{
  // Set the ERV errors to zero
  setErvError(Eigen::Vector3d::Zero());
  setPrevErv(Eigen::Vector3d::Zero());
  sigma_qk1_valid = false;

  // Update the filter's prediction based on the IMU data
  predictUKF(alpha, beta);

  // Update the quaternion from the ERV updates
  q_w_i = sigma_qk1_0;
  sigma_qk1_valid = false;

  Eigen::Quaterniond q_error = rotVecToQuat(getErvError());
  q_w_i = q_error*q_w_i;
  q_w_i.normalize();

  return;
}


/*
 * Check the state covariance matrix for unusual values and sets a default value if unusual values are found
 */
bool ImuVo::checkInitialization(void)
{
#ifdef POSITION
  bool errorFlag = false;
  if ( ( P.block<3,3>(POSITION,POSITION).diagonal().array() <= 0).any() )
  {
    errorFlag = true;
    ROS_ERROR_STREAM( "Error in POSITION initialization");
    std::cerr <<  P.block<3,3>(POSITION,POSITION).diagonal().transpose() << std::endl;
    P.block<3,3>(POSITION,POSITION) = Eigen::Matrix3d::Identity();
    std::cerr <<  P.block<3,3>(POSITION,POSITION).diagonal().transpose() << std::endl;

  }
#endif

#ifdef ERV
  if ( ( P.block<3,3>(ERV,ERV).diagonal().array() <= 0).any() )
  {
    errorFlag = true;
    ROS_ERROR_STREAM( "Error in ERV initialization");
    std::cerr <<  P.block<3,3>(ERV,ERV).diagonal().transpose() << std::endl;
    P.block<2,2>(ERV,ERV) = Eigen::Matrix2d::Identity()* pow(10 * M_PI/180,2);
    //     P(ERV+2,ERV+2) =  pow(30 * M_PI/180,2);
    P(ERV+2,ERV+2) =  pow(70 * M_PI/180,2);
    std::cerr <<  P.block<3,3>(ERV,ERV).diagonal().transpose() << std::endl;

  }
#endif

#ifdef VELOCITY
  if ( ( P.block<3,3>(VELOCITY,VELOCITY).diagonal().array() <= 0).any() )
  {
    errorFlag = true;
    ROS_ERROR_STREAM( "Error in VELOCITY initialization");
    std::cerr <<  P.block<3,3>(VELOCITY,VELOCITY).diagonal().transpose() << std::endl;
    P.block<3,3>(VELOCITY,VELOCITY) = Eigen::Matrix3d::Identity();
    std::cerr <<  P.block<3,3>(VELOCITY,VELOCITY).diagonal().transpose() << std::endl;

  }
#endif

#ifdef BIAS_ACCEL
  if ( ( P.block<3,3>(BIAS_ACCEL,BIAS_ACCEL).diagonal().array() <= 0).any() )
  {
    errorFlag = true;
    ROS_ERROR_STREAM( "Error in BIAS_ACCEL initialization");
    std::cerr <<  P.block<3,3>(BIAS_ACCEL,BIAS_ACCEL).diagonal().transpose() << std::endl;
    P.block<3,3>(BIAS_ACCEL,BIAS_ACCEL) = Eigen::Matrix3d::Identity();
    std::cerr <<  P.block<3,3>(BIAS_ACCEL,BIAS_ACCEL).diagonal().transpose() << std::endl;

  }
#endif

#ifdef BIAS_GYRO
  if ( ( P.block<3,3>(BIAS_GYRO,BIAS_GYRO).diagonal().array() <= 0).any() )
  {
    errorFlag = true;
    ROS_ERROR_STREAM( "Error in BIAS_GYRO initialization");
    std::cerr <<  P.block<3,3>(BIAS_GYRO,BIAS_GYRO).diagonal().transpose() << std::endl;
    P.block<3,3>(BIAS_GYRO,BIAS_GYRO) = Eigen::Matrix3d::Identity();
    std::cerr <<  P.block<3,3>(BIAS_GYRO,BIAS_GYRO).diagonal().transpose() << std::endl;

  }
#endif

#ifdef PREV_POS
  if ( ( P.block<3,3>(PREV_POS,PREV_POS).diagonal().array() <= 0).any() )
  {
    errorFlag = true;
    ROS_ERROR_STREAM( "Error in PREV_POS initialization");
    std::cerr <<  P.block<3,3>(PREV_POS,PREV_POS).diagonal().transpose() << std::endl;
    P.block<3,3>(PREV_POS,PREV_POS) = Eigen::Matrix3d::Identity();
    std::cerr <<  P.block<3,3>(PREV_POS,PREV_POS).diagonal().transpose() << std::endl;

  }
#endif

#ifdef PREV_ERV
  if ( ( P.block<3,3>(PREV_ERV,PREV_ERV).diagonal().array() <= 0).any() )
  {
    errorFlag = true;
    ROS_ERROR_STREAM( "Error in PREV_ERV initialization");
    std::cerr <<  P.block<3,3>(PREV_ERV,PREV_ERV).diagonal().transpose() << std::endl;
    P.block<3,3>(PREV_ERV,PREV_ERV) =  P.block<3,3>(ERV,ERV);
    std::cerr <<  P.block<3,3>(PREV_ERV,PREV_ERV).diagonal().transpose() << std::endl;

  }
#endif

  return errorFlag;

}

bool ImuVo::correctionQuaternionWrapper(Eigen::VectorXd z, Eigen::MatrixXd R)
{

  if ( ( R.diagonal().array() <= 0).any() )
  {
    ROS_ERROR_STREAM( "Error in covariance matrix R!");
    ROS_ERROR_STREAM( "ROS convention: Zeros in the diagonal mean that the covariance is unknown. Please set the covariance properly.");
    ROS_ERROR_STREAM( "ROS convention: Negative numbers in the diagonal mean that the measurement is invalid.");
    ROS_ERROR_STREAM( "The UKF is not able to properly handle either of these situations.");
    std::cerr << "R:" << std::endl;
    std::cerr << R << std::endl;
  }

  // Set the ERV errors to zero
  setErvError(Eigen::Vector3d::Zero());
  setPrevErv(Eigen::Vector3d::Zero());

  // Correct the filter based on the VO data
  bool successFlag = correctUKF(z, R, alpha, beta);

  if (successFlag)
  {
    // TODO How to handle error quaternions in correction step?
    // Update the quaternion from the ERV updates
    Eigen::Quaterniond qwi_error = rotVecToQuat(getErvError());
    q_w_i = qwi_error*q_w_i;
    q_w_i.normalize();

    Eigen::Quaterniond prev_qwi_error = rotVecToQuat(getPrevErv());
    prev_q_w_i = prev_qwi_error*prev_q_w_i;
    prev_q_w_i.normalize();
  }

  return successFlag;
}

bool ImuVo::copyCovar(int src, int dst, Eigen::MatrixXd &mat)
{

  mat.row(dst) = mat.row(src);
  mat.col(dst) = mat.col(src);

  return true;
}
